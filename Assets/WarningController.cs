﻿using System;
using System.Collections;
using System.Collections.Generic;
using ClickerFramework.Extensions;
using UnityEngine;

public class WarningController : MonoBehaviour
{
    private bool _wasVisible = false;
    private RectTransform _rect;
    private Camera _main;

    private void Start()
    {
        _rect = GetComponent<RectTransform>();
        _main = Camera.main;
    }
    
    private void LateUpdate()
    {
        if (_rect.IsVisibleFrom(_main))
        {
            if (!_wasVisible)
                _wasVisible = true;
        }
        else
        {
            if (_wasVisible)
            {
                _wasVisible = false;
                gameObject.SetActive(false);
            }
        }
    }
    
}
