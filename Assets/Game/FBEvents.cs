﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AppsFlyerSDK;
using ClickerFramework.Multilanguage;
using Facebook.Unity;
using UnityEngine;

namespace Game
{
    public static class Telemetry
    {
        private static MonoBehaviour _worker; 
        
        public static string LevelComplete = "level_complete";
        public static string FirstOpen = "first_app_open";
        public static string AppOpen = "app_open";
        public static string EnemyDie = "enemy_die";
        public static string ToolUpgrade = "tool_upgrade";
        public static string HeroBuy = "hero_buy";
        public static string ArtifactCollected = "artifact_collected";

        public static void Init(MonoBehaviour worker) => _worker = worker;
        
        public static void SendEvent(string name, Dictionary<string, object> data)
        {
            //_worker.StartCoroutine(SendEventAsync(name, data));
            FB.LogAppEvent(name, null, data);
            if (data==null)
                data = new Dictionary<string, object>();
            if (PlayerPrefs.HasKey("af_conversionData") &&
                !string.IsNullOrEmpty(PlayerPrefs.GetString("af_conversionData")))
            {
                var af_data = AppsFlyer.CallbackStringToDictionary(PlayerPrefs.GetString("af_conversionData"));
                if (af_data.Count > 0)
                    foreach (var item in af_data.Where(item =>
                        !data.ContainsKey(item.Key)))
                    {
                        data.Add(item.Key, item.Value);
                    }
            }

            Amplitude.Instance.logEvent(name, data);
            var afData = data.ToDictionary(key => key.Key, key => key.Value.ToString());
            
            AppsFlyer.sendEvent(name,afData);
            /*
            foreach (var o in data)
            {
                //Debug.Log($"Send {o.Key} {o.Value}");
            }*/
        }

        public static void SetAmplitudeUserProperty(string name, string value)
        {
            Amplitude.Instance.setUserProperty(name, value);
        }
        

        private static IEnumerator SendEventAsync(string name, Dictionary<string, object> data)
        {
            yield return new WaitForEndOfFrame();
            FB.LogAppEvent(name, null, data);
            Amplitude.Instance.logEvent(name, data);
        }
        
    }
    
    public static class TelemetryEventParams
    {
        public static string name = "name";
        public static string count = "count";
    }
}