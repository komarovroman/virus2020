using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AppsFlyerSDK;
using Game;
using UnityEngine.SceneManagement;

// This class is intended to be used the the AppsFlyerObject.prefab

public class AppsFlyerObjectScript : MonoBehaviour , IAppsFlyerConversionData
{

    public static Dictionary<string, object> ConversionDataDictionary = new Dictionary<string, object>();
    
    // These fields are set from the editor so do not modify!
    //******************************//
    public string devKey;
    public string appID;
    public bool isDebug;
    public bool getConversionData;
    //******************************//

    private bool wasSent = false;
    
    void Start()
    {
        // These fields are set from the editor so do not modify!
        //******************************//
        AppsFlyer.setIsDebug(isDebug);
        AppsFlyer.initSDK(devKey, appID, getConversionData ? this : null);
        //******************************//

        AppsFlyer.startSDK();
    }

    void Update()
    {

    }

    // Mark AppsFlyer CallBacks
    public void onConversionDataSuccess(string conversionData)
    {
        AppsFlyer.AFLog("didReceiveConversionData", conversionData);
        
        if (!string.IsNullOrEmpty(conversionData))
        {
            bool f = false;
                var af_data = AppsFlyer.CallbackStringToDictionary(conversionData);
                    if (af_data.ContainsKey("media_source"))
                        Telemetry.SetAmplitudeUserProperty("[AppsFlyer] SourceID", (string) af_data["media_source"]);
                    if (af_data.ContainsKey("campaign"))
                        Telemetry.SetAmplitudeUserProperty("[AppsFlyer] CampaignID", (string) af_data["campaign"]);
                    Debug.Log($"Started first time: {PlayerPrefs.GetString("af_conversionData")}");
            if (!PlayerPrefs.HasKey("af_conversionData"))
            {
                f = true;
            }
            
            PlayerPrefs.SetString("af_conversionData", conversionData);
            PlayerPrefs.Save();
            
            if (f)
            {
                Telemetry.SendEvent(Telemetry.FirstOpen, null);
                Telemetry.SendEvent(Telemetry.AppOpen, null);
            }

            
            
        }
        //ConversionDataDictionary = AppsFlyer.CallbackStringToDictionary(conversionData);
        // add deferred deeplink logic here
        
    }

    public void onConversionDataFail(string error)
    {
        AppsFlyer.AFLog("didReceiveConversionDataWithError", error);
    }

    public void onAppOpenAttribution(string attributionData)
    {
        AppsFlyer.AFLog("onAppOpenAttribution", attributionData);
        Dictionary<string, object> attributionDataDictionary = AppsFlyer.CallbackStringToDictionary(attributionData);
        // add direct deeplink logic here
    }

    public void onAppOpenAttributionFailure(string error)
    {
        AppsFlyer.AFLog("onAppOpenAttributionFailure", error);
    }
}
