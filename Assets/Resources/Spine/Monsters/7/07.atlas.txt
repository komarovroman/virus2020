
07.png
size: 1372,1036
format: RGBA8888
filter: Linear,Linear
repeat: none
body
  rotate: true
  xy: 2, 417
  size: 617, 820
  orig: 617, 820
  offset: 0, 0
  index: -1
eye_l
  rotate: true
  xy: 2, 2
  size: 110, 116
  orig: 110, 116
  offset: 0, 0
  index: -1
eye_r
  rotate: true
  xy: 1151, 8
  size: 94, 98
  orig: 94, 98
  offset: 0, 0
  index: -1
eyelid
  rotate: false
  xy: 714, 300
  size: 108, 115
  orig: 108, 115
  offset: 0, 0
  index: -1
hand_1
  rotate: true
  xy: 1185, 754
  size: 280, 181
  orig: 280, 181
  offset: 0, 0
  index: -1
hand_2
  rotate: false
  xy: 2, 114
  size: 492, 301
  orig: 492, 301
  offset: 0, 0
  index: -1
hand_3
  rotate: false
  xy: 824, 51
  size: 325, 621
  orig: 325, 621
  offset: 0, 0
  index: -1
hand_4
  rotate: false
  xy: 1151, 104
  size: 177, 273
  orig: 177, 274
  offset: 0, 0
  index: -1
hand_5
  rotate: true
  xy: 824, 674
  size: 360, 359
  orig: 360, 359
  offset: 0, 0
  index: -1
hand_6
  rotate: true
  xy: 496, 9
  size: 406, 216
  orig: 406, 216
  offset: 0, 0
  index: -1
mouth
  rotate: false
  xy: 1151, 379
  size: 219, 293
  orig: 219, 293
  offset: 0, 0
  index: -1
pupil_l
  rotate: true
  xy: 1185, 725
  size: 27, 32
  orig: 27, 32
  offset: 0, 0
  index: -1
pupil_r
  rotate: true
  xy: 1219, 725
  size: 27, 31
  orig: 27, 31
  offset: 0, 0
  index: -1
