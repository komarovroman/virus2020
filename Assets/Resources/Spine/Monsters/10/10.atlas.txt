
10.png
size: 2048,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
Barb_l1
  rotate: true
  xy: 411, 16
  size: 128, 99
  orig: 128, 99
  offset: 0, 0
  index: -1
Barb_l2
  rotate: true
  xy: 966, 26
  size: 121, 160
  orig: 121, 160
  offset: 0, 0
  index: -1
Barb_r1
  rotate: false
  xy: 2, 4
  size: 157, 140
  orig: 157, 140
  offset: 0, 0
  index: -1
Barb_r2
  rotate: false
  xy: 1216, 851
  size: 108, 171
  orig: 108, 171
  offset: 0, 0
  index: -1
Body
  rotate: true
  xy: 2, 146
  size: 876, 859
  orig: 876, 859
  offset: 0, 0
  index: -1
Eye_l
  rotate: true
  xy: 161, 14
  size: 130, 123
  orig: 130, 123
  offset: 0, 0
  index: -1
Eye_r
  rotate: true
  xy: 286, 14
  size: 130, 123
  orig: 130, 123
  offset: 0, 0
  index: -1
Eyelid_l1
  rotate: true
  xy: 1074, 240
  size: 158, 77
  orig: 158, 77
  offset: 0, 0
  index: -1
Eyelid_l2
  rotate: false
  xy: 1074, 175
  size: 120, 63
  orig: 120, 63
  offset: 0, 0
  index: -1
Eyelid_r1
  rotate: true
  xy: 894, 2
  size: 144, 70
  orig: 144, 70
  offset: 0, 0
  index: -1
Eyelid_r2
  rotate: false
  xy: 1112, 780
  size: 119, 67
  orig: 119, 67
  offset: 0, 0
  index: -1
Hair1
  rotate: true
  xy: 863, 545
  size: 224, 163
  orig: 224, 163
  offset: 0, 0
  index: -1
Hair2
  rotate: false
  xy: 1056, 400
  size: 74, 178
  orig: 74, 178
  offset: 0, 0
  index: -1
Leg1
  rotate: false
  xy: 1128, 21
  size: 94, 152
  orig: 94, 152
  offset: 0, 0
  index: -1
Leg10
  rotate: true
  xy: 1028, 580
  size: 194, 92
  orig: 194, 92
  offset: 0, 0
  index: -1
Leg11
  rotate: false
  xy: 512, 17
  size: 183, 127
  orig: 183, 127
  offset: 0, 0
  index: -1
Leg2
  rotate: false
  xy: 863, 336
  size: 101, 207
  orig: 101, 207
  offset: 0, 0
  index: -1
Leg3
  rotate: false
  xy: 863, 148
  size: 97, 186
  orig: 97, 186
  offset: 0, 0
  index: -1
Leg4
  rotate: false
  xy: 1022, 814
  size: 88, 208
  orig: 88, 208
  offset: 0, 0
  index: -1
Leg5
  rotate: true
  xy: 697, 21
  size: 123, 195
  orig: 123, 195
  offset: 0, 0
  index: -1
Leg6
  rotate: true
  xy: 962, 149
  size: 185, 110
  orig: 185, 110
  offset: 0, 0
  index: -1
Leg7
  rotate: false
  xy: 966, 336
  size: 88, 207
  orig: 88, 207
  offset: 0, 0
  index: -1
Leg8
  rotate: false
  xy: 1112, 849
  size: 102, 173
  orig: 102, 173
  offset: 0, 0
  index: -1
Leg9
  rotate: false
  xy: 863, 771
  size: 157, 251
  orig: 157, 251
  offset: 0, 0
  index: -1
Mouth
  rotate: true
  xy: 1122, 607
  size: 171, 85
  orig: 171, 85
  offset: 0, 0
  index: -1
Pupil_l
  rotate: false
  xy: 1022, 776
  size: 30, 36
  orig: 30, 36
  offset: 0, 0
  index: -1
Pupil_r
  rotate: false
  xy: 1054, 776
  size: 30, 36
  orig: 30, 36
  offset: 0, 0
  index: -1
