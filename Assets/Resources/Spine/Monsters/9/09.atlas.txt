
09.png
size: 2048,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
Body
  rotate: false
  xy: 2, 276
  size: 743, 744
  orig: 743, 744
  offset: 0, 0
  index: -1
Browl_l
  rotate: true
  xy: 950, 566
  size: 177, 150
  orig: 177, 150
  offset: 0, 0
  index: -1
Browl_r
  rotate: false
  xy: 1066, 421
  size: 168, 143
  orig: 168, 143
  offset: 0, 0
  index: -1
Eye_l
  rotate: true
  xy: 1151, 278
  size: 141, 121
  orig: 141, 121
  offset: 0, 0
  index: -1
Eye_r
  rotate: true
  xy: 1032, 279
  size: 137, 117
  orig: 137, 117
  offset: 0, 0
  index: -1
Eye_socket_l
  rotate: false
  xy: 2, 2
  size: 228, 272
  orig: 228, 272
  offset: 0, 0
  index: -1
Eye_socket_r
  rotate: false
  xy: 747, 415
  size: 201, 295
  orig: 207, 295
  offset: 0, 0
  index: -1
Eyelid_l
  rotate: false
  xy: 804, 4
  size: 34, 34
  orig: 34, 34
  offset: 0, 0
  index: -1
Eyelid_r
  rotate: false
  xy: 840, 4
  size: 34, 34
  orig: 34, 34
  offset: 0, 0
  index: -1
Hand_l
  rotate: true
  xy: 804, 40
  size: 234, 217
  orig: 235, 217
  offset: 0, 0
  index: -1
Hand_r
  rotate: false
  xy: 1023, 46
  size: 213, 228
  orig: 213, 228
  offset: 0, 0
  index: -1
Leg_1
  rotate: false
  xy: 232, 14
  size: 194, 260
  orig: 194, 260
  offset: 0, 0
  index: -1
Leg_2
  rotate: false
  xy: 428, 30
  size: 186, 244
  orig: 186, 244
  offset: 0, 0
  index: -1
Leg_3
  rotate: true
  xy: 747, 276
  size: 137, 283
  orig: 137, 283
  offset: 0, 0
  index: -1
Leg_4
  rotate: false
  xy: 747, 712
  size: 143, 308
  orig: 143, 308
  offset: 0, 0
  index: -1
Leg_5
  rotate: false
  xy: 616, 30
  size: 186, 244
  orig: 186, 244
  offset: 0, 0
  index: -1
Leg_6
  rotate: true
  xy: 892, 745
  size: 275, 220
  orig: 275, 220
  offset: 0, 0
  index: -1
Mouth
  rotate: true
  xy: 1102, 579
  size: 164, 89
  orig: 164, 89
  offset: 0, 0
  index: -1
eyelid
  rotate: true
  xy: 950, 418
  size: 146, 114
  orig: 146, 114
  offset: 0, 0
  index: -1
