﻿using System;
using System.Collections;
using System.Collections.Generic;
using BreakInfinity;
using ClickerFramework;
using ClickerFramework.Extensions;
using UnityEngine;
using UnityEngine.Diagnostics;
using UnityEngine.UI;

public class ExchangeWindow : MonoBehaviour
{
    [SerializeField] private InputField input;
    [SerializeField] private Text money;
    [SerializeField] private GameObject errorWindow;

    private bool _opened = false;
    private TouchScreenKeyboard _keyboard;
    
    public void Exchange()
    {
        if (string.IsNullOrEmpty(input.text) || string.IsNullOrWhiteSpace(input.text))
        {
            return;
        }
        
        var val = int.Parse(input.text);
        if (val > World.Instance.CurrentCrystals)
        {
            errorWindow.SetActive(true);
            Close();
            return;
        }

        World.Instance.CurrentCrystals -= val;
        var n = World.Instance.Save.GetReachedLevel()/10;
        World.Instance.CurrentMoney += NumberHelper.BigGeomProgression(40, 10, n) * val;
        
        AudioManager.Instance.PlaySound("earn_crystals");
        Close();
    }

    private void Update()
    {
        /*if (_opened)
        {
            _keyboard ??= TouchScreenKeyboard.Open("", TouchScreenKeyboardType.NumberPad, false, false, false, false);
            
        }*/
    }

    public void Show()
    {
        if (string.IsNullOrEmpty(input.text) || string.IsNullOrWhiteSpace(input.text))
        {
            money.text = "0";
            return;
        }

        
        var val = int.Parse(input.text);
        var n = World.Instance.Save.GetReachedLevel()/10;
        money.text = NumberHelper.FormatNumber(NumberHelper.BigGeomProgression(40, 10, n) * val);
        _opened = true;
    }
    
    public void Close()
    {
        gameObject.SetActive(false);
        _opened = false;
    }
}
