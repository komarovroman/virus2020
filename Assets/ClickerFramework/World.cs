﻿using System;
using BreakInfinity;
using ClickerFramework.Abstract;
using ClickerFramework.Extensions;
using ClickerFramework.Multilanguage;
using Game;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace ClickerFramework
{
    /// <summary>
    /// Main game class
    /// </summary>
    public class World : MonoBehaviour
    {
        public static World Instance;

        /// <summary>
        /// Current level
        /// </summary>
        public int level = 0;

        /// <summary>
        /// Multipliers that using for progressions
        /// </summary>
        public float enemyHealthMultiplier, bossHealthMultiplier, moneyMultiplier;

        /// <summary>
        /// Multipliers that using by skills, artifacts and heros
        /// </summary>
        [NonSerialized] public float GlobalHealthMultiplier = 1,
            GlobalMoneyMultiplier = 1,
            GlobalDamageMultiplier = 1,
            GlobalPassiveDamageMultiplier = 1,
            GlobalTravelTimeMultiplier = 1,
            GlobalTravelRewardsMultiplier = 1;


        private BigDouble _currentMoney = 0;
        private BigDouble _currentCrystals = 0;

        /// <summary>
        /// Current money
        /// </summary>
        public BigDouble CurrentMoney
        {
            get => _currentMoney;
            set
            {
                _currentMoney = value;
                Save.GameStatistic.TotalMoney += value;
                Save.SetMoney(value);
                money.text = NumberHelper.FormatNumber(CurrentMoney);
            }
        }

        /// <summary>
        /// Current crystals
        /// </summary>
        public BigDouble CurrentCrystals
        {
            get => _currentCrystals;
            set
            {
                _currentCrystals = value;
                Save.SetCrystals(value);
                crystals.text = NumberHelper.FormatNumber(CurrentCrystals);
            }
        }

        /// <summary>
        /// Max money that we can get from enemy on current level
        /// </summary>
        [SerializeField] public BigDouble maxMoney;

        /// <summary>
        /// Base maximum money that using for inscreasing
        /// </summary>
        [SerializeField] public BigDouble baseMaxMoney;


        /// <summary>
        /// Labels for main menu panel
        /// </summary>
        [SerializeField] private Text activeDMG, passiveDMG, money, crystals;

        /// <summary>
        /// Window for idle rewards
        /// </summary>
        [SerializeField] private GameObject idleRewardWindow;

        /// <summary>
        /// Text of reward
        /// </summary>
        [SerializeField] private TextMeshProUGUI idleRewardText;

        /// <summary>
        /// Button for claiming reward
        /// </summary>
        [SerializeField] private Button idleRewardClaimButton;

        /// <summary>
        /// Gameobject configured for damage text
        /// </summary>
        public TextBalloon textBalloon;

        /// <summary>
        /// Growth used for damage increasing
        /// </summary>
        public BigDouble playerDamageGrowth;

        /// <summary>
        /// Save data
        /// </summary>
        [NonSerialized] public Save Save;

        internal Menu MainMenu;

        public delegate void OnLevelChangeHandler(int level);

        /// <summary>
        /// Event that invokes on level changing 
        /// </summary>
        public event OnLevelChangeHandler OnLevelChange;

        private void Awake()
        {
            Instance = this;
            Telemetry.Init(this);
            //loading save data
            Save = GetComponent<Save>();
        }

        private void Start()
        {
            LanguageLoader.Instance.LoadLang();
            
            //Setup loaded save data
            level = Save.GetReachedLevel() - 1;
            CurrentMoney = Save.GetMoney();
            CurrentCrystals = Save.GetCrystals();
            CalculateMoneyMultiplier();
            maxMoney = CalculateMaxMoney();
            
            TravelController.Instance.Restore();

            SetIdleRewardWindow();

            MainMenu = GameObject.Find("Canvas").GetComponent<Menu>();
        }

        private void Update()
        {
#if !UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Save.SaveAllNow();
                Application.Quit();
            }
#else
            if (Input.GetKey(KeyCode.Escape)) AddMoney(1000000);
#endif
        }

        public void CalculateMoneyMultiplier()
        {
            moneyMultiplier = NumberHelper.GeomProgression(1.45f, 1.0000000000005f, (level / 10));
        }
        
        private void LateUpdate()
        {
            Save.SetTime(DateTime.Now.TimeOfDay);
        }


        public void IncMoneyMultiplier() => maxMoney *= 1.5f;
        
        /// <summary>
        /// Setup and shows idle reward window
        /// </summary>
        private void SetIdleRewardWindow()
        {
            var reward = CalculateIdleDamage();
            if (reward <= 0) return;

            idleRewardText.text = NumberHelper.FormatNumber(reward);
            idleRewardClaimButton.onClick.AddListener(() =>
            {
                AddMoney(reward);
                idleRewardWindow.SetActive(false);
            });
            idleRewardWindow.SetActive(true);
        }


        /// <summary>
        /// Calculate money that tools can earn by killing enemies in background
        /// </summary>
        /// <returns>Total reward</returns>
        private BigDouble CalculateIdleDamage()
        {
            BigDouble dmg = 0;
            if (Save.GetTime() == TimeSpan.Zero) return -1;
            foreach (var attacker in Player.Instance.attackers)
            {
                if (attacker.isActive && !attacker.isInCompany)
                {
                    dmg += attacker.Damage * GlobalPassiveDamageMultiplier;
                }
            }

            dmg = dmg.Round();
            var dmgPerSecond = (dmg*Time.fixedDeltaTime);
            var time = (DateTime.Now.TimeOfDay.TotalSeconds - Save.GetTime().TotalSeconds);
            Debug.Log($"{DateTime.Now.TimeOfDay.TotalSeconds} - {Save.GetTime().TotalSeconds} = {time}");
            var totalDamage = dmgPerSecond * time;
            Debug.Log($"{dmgPerSecond} * {time} = {totalDamage}");

            var enemyCount = (totalDamage / CalculateEnemyHealth());
            if (!BigDouble.IsInfinity(enemyCount) && enemyCount > 20)
                enemyCount = 20;

            var totalMoney = CalculateEarnedMoney() * enemyCount;

            //TODO: Can be improved
            //Check for bad cases and debug
            if (enemyCount >= 1 && !BigDouble.IsNaN(totalMoney) && !BigDouble.IsInfinity(totalMoney) && totalMoney > 0)
            {
                Debug.Log(
                    $"You earned {totalMoney} in idle: {enemyCount} {dmg} {totalDamage} {dmgPerSecond} {time} {CalculateEarnedMoney()}");
                return totalMoney;
            }

            Debug.Log($"You earned nothing {enemyCount} {dmg} {totalDamage} {dmgPerSecond} {time}");
            return -1;
        }

        /// <summary>
        /// Changing level (mainly for map)
        /// </summary>
        /// <param name="newLevel">level to go</param>
        public void LevelChange(int newLevel)
        {
            level = newLevel;
            //healthMultiplier += healthMultiplier;
            CalculateMoneyMultiplier();
            maxMoney = CalculateMaxMoney();

            Save.SetReachedLevel(level);
            OnLevelChange.Invoke(level);

          
            
            CalculateCountryStatistics();
        }

        
        /// <summary>
        /// Calculate statistics about cleaned countries
        /// </summary>
        private void CalculateCountryStatistics()
        {
            switch (level)
            {
                case 29: //12+6+3+5+3
                    Save.GameStatistic.ClearedFirstPartOfCountries = true;
                    //Save.SaveAll();
                    break;
                case 34: //1+1+1+1+1
                    Save.GameStatistic.ClearedSecondPartOfCountries = true;
                    //Save.SaveAll();
                    break;
                case 95: //1+1+9+32+1+2+8+2+3+4
                    Save.GameStatistic.ClearedThirdPartOfCountries = true;
                    //Save.SaveAll();
                    break;
            }

        }

        /// <summary>
        /// Reset game save data
        /// </summary>
        public void ResetGame()
        {
            Debug.Log("Reset save");
            Save.RestoreDefaults();
            SceneManager.LoadScene(0);
        }
        
        /// <summary>
        /// Calculates enemy health progression
        /// </summary>
        /// <returns>Enemy health on selected level</returns>
        public BigDouble CalculateEnemyHealth()
        {
            /*var b = BigDouble.Pow(enemyHealthMultiplier, level);
            var a = 5 * b;
            return a;*/
            return NumberHelper.BigGeomProgression(5, enemyHealthMultiplier, level);
        }

        /// <summary>
        /// Calculates money progression
        /// </summary>
        /// <returns>Money that can be earned on selected level</returns>
        public BigDouble CalculateMaxMoney()
        {
            /*var b = BigDouble.Pow(moneyMultiplier, level);
            var a = baseMaxMoney * b;
            return a;*/
            //return baseMaxMoney * moneyMultiplier * (level - 1);
            return NumberHelper.BigGeomProgression(baseMaxMoney, moneyMultiplier, level);
        }
        
        /// <summary>
        /// Calculating money
        /// </summary>
        /// <returns>money</returns>
        public BigDouble CalculateEarnedMoney() => maxMoney * /*Random.Range(1f, 1.5f) **/ GlobalMoneyMultiplier;

        /// <summary>
        /// Updates total tools damage 
        /// </summary>
        /// <param name="damage">total damage</param>
        public void SetPassiveDamage(BigDouble damage)
        {
            passiveDMG.text = NumberHelper.FormatNumber(damage);
        }

        /// <summary>
        /// Updates player damage
        /// </summary>
        /// <param name="damage">damage</param>
        public void SetActiveDamage(BigDouble damage)
        {
            activeDMG.text = NumberHelper.FormatNumber(damage);
        }

        /// <summary>
        /// Adds money
        /// </summary>
        /// <param name="money">new money</param>
        public void AddMoney(BigDouble money) => CurrentMoney += money;


        /// <summary>
        /// Adds crystals
        /// </summary>
        /// <param name="crystals">new crystals</param>
        public void AddCrystals(BigDouble crystals) => CurrentCrystals += crystals;
    }
}