﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ClickerFramework.Extensions
{
    [System.Serializable]
    public class GameObjectDictionary
    {
        [SerializeField]
        public GameObjectDictionaryElement[] elements;
        public GameObject this[string key]
        {
            get
            {
                foreach (var element in elements)
                {
                    if (element.Key == key)
                        return element.Value;
                }

                throw new Exception($"GameObjectDictionary: {key} Not found");
            }
        }
    }

    [System.Serializable]
    public class GameObjectDictionaryElement
    {
        [SerializeField]
        public string Key;
        [SerializeField]
        public GameObject Value;
    }

    
}