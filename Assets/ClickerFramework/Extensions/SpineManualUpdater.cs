using UnityEngine;
using Spine.Unity;

namespace ClickerFramework.Extensions
{

    public class SpineManualUpdater : MonoBehaviour {

        public SkeletonMecanim skeletonAnimation;

        [Range(1/60f, 1f/8f)] // slider from 60fps to 8fps
        public float timeInterval = 1f / 24f; // 24fps

        private float deltaTime;

#if UNITY_EDITOR
        void OnValidate () {
            if (skeletonAnimation == null)
                skeletonAnimation = GetComponent<SkeletonMecanim>();
        }
#endif

        void Start () {
            if (skeletonAnimation == null)
                skeletonAnimation = GetComponent<SkeletonMecanim>();

            skeletonAnimation.Initialize(false);
            skeletonAnimation.clearStateOnDisable = false;
            skeletonAnimation.enabled = false;
            ManualUpdate();
        }

        void Update () {
            deltaTime += Time.deltaTime;
            if (deltaTime >= timeInterval)
                ManualUpdate();
        }

        void ManualUpdate () {
            skeletonAnimation.Update();
            skeletonAnimation.LateUpdate();
            deltaTime -= timeInterval; //deltaTime = deltaTime % timeInterval; // optional time accuracy.
        }

    }
}