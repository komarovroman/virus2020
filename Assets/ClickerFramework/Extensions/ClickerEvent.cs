using UnityEngine.Events;

namespace ClickerFramework.Extensions
{
    public class ClickerIntEvent: UnityEvent<int>
    {
        
    }

    public class ClickerBoolEvent : UnityEvent<bool>
    {
        
    }
}