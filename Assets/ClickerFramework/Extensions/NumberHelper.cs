﻿using System;
using BreakInfinity;
using UnityEngine;

namespace ClickerFramework.Extensions
{
    public static class NumberHelper
    {
        private static readonly string[] Suffixes = {"", "K", "M", "B", "T"};

        public static string FormatNumber(BigDouble value)
        {
            var num = 0;
            while (value >= 1000d)
            {
                num++;
                value /= 1000d;
            }

            if (num > 0)
            {
                if (num<Suffixes.Length)
                    return value.ToString("F1") + Suffixes[num];
                else
                {
                    num -= Suffixes.Length;
                    var suffixA = (char) (097 + (num / 25));
                    var suffixB = (char) (097 + (LoopNumber(num,25)));
                    return value.ToString("F1") + suffixA+suffixB;
                }
            }
            else return value.ToString("F0");
        }

        public static float GeomProgression(float a, float q, int n) 
        {
            var b = (float) Math.Pow(q, n);
            return a * b;
        }
        
        public static BigDouble BigGeomProgression(BigDouble a, BigDouble q, int n) 
        {
            var b = BigDouble.Pow(q, n);
            return a * b;
        }

        public static BigDouble BigArithmeticProgression(BigDouble a, BigDouble d, int n)
        {
            var b = 2f * a + (n - 1) * d;
            return (b / 2f) * n;
        }

        public static float LoopNumber(float number, float maxValue)
        {
            return number >= maxValue ? number % (maxValue) : number;
        }
        
    }
}