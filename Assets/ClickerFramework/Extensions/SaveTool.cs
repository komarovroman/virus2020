﻿#if UNITY_EDITOR
using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ClickerFramework.Extensions
{
    public class SaveTool : MonoBehaviour
    {
        [MenuItem("Tools/Reset game")]
        public static void ResetGame()
        {
            PlayerPrefs.DeleteAll();
            //SceneManager.LoadScene(0);
        }
    }
}
#endif