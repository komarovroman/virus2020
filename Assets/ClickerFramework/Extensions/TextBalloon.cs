﻿using System.Collections;
using BreakInfinity;
using UnityEngine;
using UnityEngine.UI;

namespace ClickerFramework.Extensions
{
    //[RequireComponent(typeof(Canvas))]
    public class TextBalloon : MonoBehaviour
    {
        [SerializeField] private Transform canvas;
        [SerializeField] private GameObject damageText;
        // Start is called before the first frame update
        void Start()
        {
            //canvas = GetComponent<Canvas>().gameObject.transform;
        }

        // Update is called once per frame
        void Update()
        {
        
        }

        private IEnumerator MoveBalloon(string val, Color color, Transform target,  float fadeSpeed=0.75f)
        {
            var text = target.gameObject.GetComponent<Text>();
            text.text = val;
            text.color = new Color(color.r, color.g, color.b, 1);
            while (text.color.a > 0.0f)
            {
                target.Translate(Vector3.up/40);
                text.color = new Color(text.color.r, text.color.g, text.color.b, text.color.a - (Time.deltaTime * fadeSpeed));
                yield return null;
            }

            yield return new WaitForEndOfFrame();
                Destroy(target.gameObject);
        }

        private Transform CreateObject()
        {
            var g = Instantiate(damageText, canvas);
            g.GetComponent<RectTransform>().anchoredPosition = new Vector2(Random.Range(-75,75),300);
            /*RectTransformUtility.ScreenPointToLocalPointInRectangle(g.GetComponent<RectTransform>(), 
                new Vector2(Random.Range(0, Screen.width), Random.Range(0, Screen.height)), Camera.main, out var localpoint);
            g.transform.localPosition = localpoint;*/
            return g.transform;
        } 
        private Transform CreateObject(Vector2 position)
        {
            var g = Instantiate(damageText, canvas);
            RectTransformUtility.ScreenPointToLocalPointInRectangle(g.GetComponent<RectTransform>(), position, Camera.main, out var localpoint);
            g.transform.position = localpoint;
            return g.transform;
        }
        
        public void Spawn(string text, Color color, Vector2 position,  float fadeSpeed=1f)
        {
            
            StartCoroutine(MoveBalloon(text, color, CreateObject(position),fadeSpeed));
        }
    
        public void Spawn(BigDouble dmg, Color color, Vector2 position,  float fadeSpeed=1f)
        {
            StartCoroutine(MoveBalloon(NumberHelper.FormatNumber(dmg), color,CreateObject(position),fadeSpeed));
        } 
        
        public void Spawn(string text, Color color,  float fadeSpeed=1f)
        {
            StartCoroutine(MoveBalloon(text, color,CreateObject(),fadeSpeed));
        }
    
        public void Spawn(BigDouble dmg, Color color,  float fadeSpeed=1f)
        {
            StartCoroutine(MoveBalloon(NumberHelper.FormatNumber(dmg), color,CreateObject(),fadeSpeed));
        }
    }
}
