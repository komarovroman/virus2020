﻿using System;
using System.Collections.Generic;
using System.Linq;
using BreakInfinity;
using ClickerFramework.Extensions;
using ClickerFramework.Heros;
using ClickerFramework.Multilanguage;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace ClickerFramework
{
    /// <summary>
    /// Class that controls "caves mode" where player can get free crystals
    /// </summary>
    public class TravelController : MonoBehaviour
    {
        public static TravelController Instance;

        /// <summary>
        /// Default icon in slot
        /// </summary>
        [SerializeField] private Sprite defaultIcon;

        [SerializeField] private GameObject TravelWindow;
        
        /// <summary>
        /// Tool name labels
        /// </summary>
        [SerializeField] private Text[] toolNames;

        /// <summary>
        /// Tool image slots
        /// </summary>
        [SerializeField] private Image[] toolCells;

        /// <summary>
        /// Button for travel start
        /// </summary>
        [SerializeField] private Button startTravelButton;

        /// <summary>
        /// Button label (for timer)
        /// </summary>
        [SerializeField] private Text startTravelButtonLabel;

        /// <summary>
        /// Travel reward window
        /// </summary>
        [SerializeField] private GameObject rewardWindow;

        /// <summary>
        /// Travel reward text
        /// </summary>
        [SerializeField] private TextMeshProUGUI rewardText;

        /// <summary>
        /// Travel claim reward button
        /// </summary>
        [SerializeField] private Button claim;

        /// <summary>
        /// Time when travel must end
        /// </summary>
        private TimeSpan _endTime;

        /// <summary>
        /// Travel level (for time and crystals progression
        /// </summary>
        private int _travelLevel = 1;

        /// <summary>
        /// Time needed for travel in seconds
        /// </summary>
        private float _time;

        /// <summary>
        /// Current all available tool slots
        /// </summary>
        private float _toolsCount = 0;

        /// <summary>
        /// Total reward from travel
        /// </summary>
        private float _reward;

        /// <summary>
        /// Selected tools for travel
        /// </summary>
        private List<PassiveAttacker> _selected;

        /// <summary>
        /// Bools for control state 
        /// </summary>
        private bool _available, _working, _toolsSelected;

        private Hero hero;

        private void Awake()
        {
            Instance = this;
        }


        // Start is called before the first frame update
        private void Start()
        {
            _selected = new List<PassiveAttacker>();
            _travelLevel = World.Instance.Save.GetTravelLevel();
            _endTime = World.Instance.Save.GetTravelEndTime(); 
            //Debug.Log(_endTime);
            CalculateIdleTravel();
            Restore();
        }

        /// <summary>
        /// Restore travel state from save data
        /// </summary>
        public void Restore()
        {
           
            switch (_travelLevel)
            {
                case 1:
                    _toolsCount = 1;
                    break;
                case 2:
                    _toolsCount = 2;
                    break;
                default:
                    _toolsCount = 3;
                    break;
            }
           
            //Check if we in travel
            if (_endTime != TimeSpan.Zero)
            {
                if (DateTime.Now.TimeOfDay < _endTime)
                {
                    _working = _toolsSelected = true;
                    var list = World.Instance.Save.GetToolsInTravel();
                    for (var i = 0; i < list.Count; i++)
                    {
                        var tool = list[i];
                        var item = Player.Instance.attackers.FirstOrDefault(x => x.Name == tool);
                        if (item == null) throw new Exception($"Tool {tool} not found");
                        _selected.Add(item);
                        item.isInCompany = true;

                        toolCells[i].gameObject.SetActive(true);
                        toolNames[i].text = LanguageLoader.Instance.GetEntryValue("{"+item.Name+"}");
                        toolCells[i].transform.GetChild(0).gameObject.GetComponent<Image>().sprite = _selected[i].icon;
                        Debug.Log(i + " " + _selected[i].icon.name);
                    }

                    //_time = (300 + 300 * (_travelLevel - 1));
                    //_reward = (5 + 5 * (_travelLevel - 1)) * World.Instance.GlobalTravelRewardsMultiplier;
                    _reward = CalculateTravelReward();
                }
            }
            else
            {
                _working = _toolsSelected = false;
            }
        }

        /// <summary>
        /// Opening travel window
        /// </summary>
        public void OnOpen()
        {
            switch (_travelLevel)
            {
                case 1:
                    _toolsCount = 1;
                    break;
                case 2:
                    _toolsCount = 2;
                    break;
                default:
                    _toolsCount = 3;
                    break;
            }

            _available = IsAvailable();
            //Debug.Log($"av: {_available} = {Player.Instance.attackers.Count(x => x.isActive)} >= {_toolsCount} && {!_working}");
            
            startTravelButton.interactable = _available;

            //Cleanup
            if (!_toolsSelected)
            {
                for (var i = 0; i < _toolsCount; i++)
                {
                    toolCells[i].gameObject.SetActive(true);
                    toolCells[i].transform.GetChild(0).gameObject.GetComponent<Image>().sprite = defaultIcon;
                    toolNames[i].text = "";
                }
            }

            if (!_available) return;
            
            if (!_toolsSelected) SetUpNextTools();
        }

        
        
        private bool IsAvailable() => Player.Instance.attackers.Count(x => x.isActive) >= _toolsCount && !_working;
        
        /// <summary>
        /// Selecting random tools for travel
        /// </summary>
        private void SetUpNextTools()
        {
          
            switch (_travelLevel)
            {
                case 1:
                    _toolsCount = 1;
                    break;
                case 2:
                    _toolsCount = 2;
                    break;
                default:
                    _toolsCount = 3;
                    break;
            }

            if (!IsAvailable()) return;
            _toolsSelected = true;
            var arr = Player.Instance.attackers.Where(x => x.isActive).ToArray();
            _selected.Clear();
            World.Instance.Save.ClearToolsInTravel();
            var indexes = new List<int>();
            for (int i = 0; i < _toolsCount; i++)
            {
                var ind = Random.Range(0, arr.Length);
                while (indexes.Contains(ind))
                    ind = Random.Range(0, arr.Length);

                indexes.Add(ind);

                _selected.Add(arr[ind]);
                _selected[i].isInCompany = true;
                toolCells[i].gameObject.SetActive(true);
                toolCells[i].transform.GetChild(0).gameObject.GetComponent<Image>().sprite = _selected[i].icon;
                toolNames[i].text = LanguageLoader.Instance.GetEntryValue("{"+_selected[i].Name+"}");

                World.Instance.Save.AddToolToTravel(_selected[i].Name);
            }
        }

        /// <summary>
        /// Starts travel
        /// </summary>
        public void StartTravelling()
        {
            //disabling tools
            foreach (var tool in _selected)
                tool.isInCompany = true;
            
            //Calculate time and reward
            _time = CalculateTravelTime();
            _reward = CalculateTravelReward();
            //Debug.Log($"New time {DateTime.Now.TimeOfDay } {TimeSpan.FromSeconds(_time)}");
            _endTime = DateTime.Now.TimeOfDay + TimeSpan.FromSeconds(_time);
            World.Instance.Save.SetTravelEndTime(_endTime);
            _working = true;
            startTravelButton.interactable = false;
        }
        
        public void StartTravelling(TimeSpan startTime)
        {
            //disabling tools
            foreach (var tool in _selected)
                tool.isInCompany = true;
            
            //Calculate time and reward
            _time = CalculateTravelTime();
            _reward = CalculateTravelReward();
            //Debug.Log($"New time {DateTime.Now.TimeOfDay } {TimeSpan.FromSeconds(_time)}");
            _endTime = startTime + TimeSpan.FromSeconds(_time);
            World.Instance.Save.SetTravelEndTime(_endTime);
            _working = true;
            startTravelButton.interactable = false;
        }
        
        private float CalculateTravelTime() =>
            (300 + 300 * (_travelLevel - 1)) * World.Instance.GlobalTravelTimeMultiplier;

        private float CalculateTravelReward() =>
            (5 + 5 * (_travelLevel - 1)) * World.Instance.GlobalTravelRewardsMultiplier; 
        
        
        // Update is called once per frame
        private void LateUpdate()
        {
            if (_working)
            {
                //Displaying time
                if (DateTime.Now.TimeOfDay > _endTime)
                    StopTravel();
                else
                    startTravelButtonLabel.text = (DateTime.Now.TimeOfDay - _endTime).ToString("hh\\:mm\\:ss");
            }
            else
            {
                if (IsAvailable())
                {
                    if (hero == null)
                        return;
                    if (hero.delay >= 0 && DateTime.Now.TimeOfDay <
                        World.Instance.Save.GetHeroUpdateTime() + TimeSpan.FromHours(hero.delay))
                        return;
                    
                    if (!_toolsSelected) SetUpNextTools();
                    
                    StartTravelling();
                    World.Instance.Save.SetHeroUpdateTime(DateTime.Now.TimeOfDay);
                }
            }
        }

        private void CalculateIdleTravel()
        {
            if (hero == null) return;
            var now = DateTime.Now.TimeOfDay;
            var totalElapsedTime = (now - World.Instance.Save.GetTime()).TotalSeconds;
            var lastHeroUpdate = World.Instance.Save.GetHeroUpdateTime();
            var timeFromLastHeroUpdate = hero.delay > 0
                ? (lastHeroUpdate+TimeSpan.FromHours(hero.delay)).TotalSeconds
                : 0;
            var lastRunTime = World.Instance.Save.GetTime();
            var i = 0;
            var totalReward = 0f;
            
            while (totalElapsedTime >= 0)
            {
                if (i>0)
                    StartTravelling(lastRunTime);
                
                var a = (float) (_endTime - lastRunTime).TotalSeconds;
                var b = (float) timeFromLastHeroUpdate;
                totalElapsedTime -= Mathf.Max(a, b);

                Debug.Log($"{_endTime} {lastRunTime}");
                
                if (i==0)
                    StartTravelling(lastRunTime);
                
                lastHeroUpdate = lastRunTime+TimeSpan.FromHours(hero.delay);
                timeFromLastHeroUpdate = TimeSpan.FromHours(hero.delay).TotalSeconds;
                if (timeFromLastHeroUpdate < 0) timeFromLastHeroUpdate = 0;

                /*timeFromLastHeroUpdate =
                    ((lastRunTime - lastHeroUpdate)).TotalSeconds;*/

                lastRunTime += TimeSpan.FromSeconds(Mathf.Max(a, b));
                
                Debug.Log(
                    $"IDLE Travel #{i} a: {a} b: {b} totalTime {totalElapsedTime} {lastRunTime} {timeFromLastHeroUpdate}");
                if (totalElapsedTime > 0)
                {
                    //totalReward += _reward;
                    StopTravel(false);
                    totalReward += _reward;
                }

                i++;
            }

            if (totalReward > 0)
            {
                rewardText.text = totalReward.ToString();
                rewardWindow.SetActive(true);
            }

        }
        
        public void AssignHero(Hero hero)
        {
            if (this.hero == null)
            {
                this.hero = hero;
                return;
            }

            if (hero.weight > this.hero.weight)
            {
                this.hero = hero;
                World.Instance.Save.SetHeroUpdateTime(DateTime.Now.TimeOfDay);
            }
        }

        /// <summary>
        /// Stops travel and revert tools
        /// </summary>
        private void StopTravel(bool showWindow = true)
        {
            TravelWindow.SetActive(false);
            World.Instance.Save.SetTravelEndTime(TimeSpan.Zero);
            _endTime = TimeSpan.Zero;

            for (var i = 0; i < _selected.Count; i++)
            {
                _selected[i].isInCompany = false;
                Debug.Log($"You on tool {i}");
            }

            for (int i = 0; i < _toolsCount; i++)
            {
                toolCells[i].gameObject.SetActive(false);
                toolNames[i].text = "Пусто";
            }

            _working = _toolsSelected = false;

            World.Instance.Save.ClearToolsInTravel();
            _selected.Clear();

            rewardText.text = NumberHelper.FormatNumber(_reward);
            World.Instance.AddCrystals(_reward);
            claim.onClick.AddListener(() =>
            {
                rewardWindow.SetActive(false);
            });

            startTravelButtonLabel.text = "Начать ";

            _travelLevel++;
            World.Instance.Save.IncTravelLevel();

            if (showWindow)
                rewardWindow.SetActive(true);
            else
            {
                Debug.Log($"You earned {_reward}");
            }

            SetUpNextTools();
        }
    }
}