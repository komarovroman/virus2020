﻿using BreakInfinity;
using ClickerFramework.Abstract;
using UnityEngine;

namespace ClickerFramework.SkillTree
{
    /// <summary>
    /// Basic class for all skills
    /// </summary>
    public abstract class Skill : ScriptableObject, IClickerAsset
    {
        /// <summary>
        /// Skill id
        /// </summary>
        public string skillId;

        /// <summary>
        /// Skill description
        /// </summary>
        [Multiline] public string description;

        /// <summary>
        /// Skill price
        /// </summary>
        public BigDouble price;

        /// <summary>
        /// Skill parameters 
        /// </summary>
        public bool isUpdatable = false, isActive = false, itSold = false;

        /// <summary>
        /// On skill discover first time
        /// </summary>
        public virtual void OnDiscover()
        {
            isActive = true;
            World.Instance.Save.AddNewSkill(skillId);
            SkillTree.Instance.UpdateAllTree();
        }

        /// <summary>
        /// Update tick for isUpdatable
        /// </summary>
        public virtual void OnGameUpdate()
        {
        }
    }
}