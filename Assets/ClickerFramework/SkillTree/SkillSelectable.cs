﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ClickerFramework.SkillTree
{
    /// <summary>
    /// Class of UI skill dot
    /// </summary>
    [RequireComponent(typeof(Image))]
    public class SkillSelectable : MonoBehaviour, IPointerDownHandler
    {
        /// <summary>
        /// Id of linked skill
        /// </summary>
        [SerializeField] private string skillId;

        /// <summary>
        /// Skill in chain that needed to be active to activate this
        /// </summary>
        [SerializeField] private List<GameObject> skillsToBeOpened;

        /// <summary>
        /// Lines between dots
        /// </summary>
        [SerializeField] private List<GameObject> lines;

        /// <summary>
        /// Main bool variable
        /// </summary>
        [SerializeField] public bool canWeBuyIt = false;

        /// <summary>
        /// Linked skill
        /// </summary>
        private Skill _skill;

        /// <summary>
        /// Getting skill by id
        /// </summary>
        /// <returns>founded skill</returns>
        public Skill GetSkillInstance()
        {
            if (_skill == null)
            {
                _skill = SkillTree.Instance.GetSkill(skillId);
            }

            return _skill;
        }

        private void Awake()
        {
            
        }

        private void Start()
        {
            _skill = SkillTree.Instance.GetSkill(skillId);
            Colorize();
        }

        /// <summary>
        /// Sets all dots and lines between it
        /// </summary>
        private void Colorize()
        {
            if (gameObject.transform.childCount > 0)
            {
                //Debug.Log( gameObject.transform.GetChild(0).gameObject.name);
                gameObject.transform.GetChild(0).gameObject.SetActive(IsSold());
                /*if (IsSold())
                    Debug.Log(gameObject.transform.GetChild(0).gameObject.name);*/
            }

            foreach (var line in lines)
                if (line)
                    line.SetActive(canWeBuyIt);
        }

        /// <summary>
        /// On click set panel on selected skill
        /// </summary>
        /// <param name="eventData"></param>
        public void OnPointerDown(PointerEventData eventData)
        {
            SkillTree.Instance.skillPanel.SetPanel(this, _skill);
        }

        public void Discover()
        {
            _skill.OnDiscover();
        }

        /// <summary>
        /// Checking skill chain from self
        /// </summary>
        public void CheckSelf()
        {
            foreach (var item in skillsToBeOpened)
            {
                var t = item.GetComponent<SkillSelectable>();
                //Debug.Log($"{t.skillId} {t.IsSold()}");
                if (!t.IsSold())
                    return;
            }
            
            canWeBuyIt = true;
            Colorize();
        }

        /// <summary>
        /// We already bought it?
        /// </summary>
        /// <returns>\_O_O_/</returns>
        private bool IsSold()
        {
            //Debug.Log($"{_skill.skillId} {_skill.itSold}");
            return _skill.itSold;
        }
    }
}