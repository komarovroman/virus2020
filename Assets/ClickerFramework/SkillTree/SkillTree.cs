﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;

namespace ClickerFramework.SkillTree
{
    /// <summary>
    /// Class that used for controlling skill tree
    /// </summary>
    public class SkillTree : MonoBehaviour
    {
        public static SkillTree Instance;

        /// <summary>
        /// Skill panel that used for description and purchase
        /// </summary>
        public SkillDiscoverPanel skillPanel;

        /// <summary>
        /// List of all available skills
        /// </summary>
        [FormerlySerializedAs("_activeSkills")] [SerializeField]
        private List<Skill> activeSkills = new List<Skill>();

        /// <summary>
        /// All ui skill dots
        /// </summary>
        [SerializeField] private SkillSelectable[] selectableSkills;

        private void Awake()
        {
            Instance = this;
        }

        private void Start()
        {
            //Check skills chain (to know what we can buy
            foreach (var sel in selectableSkills)
            {
                var skill = sel.GetSkillInstance();
                if (World.Instance.Save.IsSkillBought(skill.skillId))
                    skill.itSold = skill.isActive = true;
            }

            //Update ui part of tree
            UpdateAllTree();
        }

        private void FixedUpdate()
        {
            //Update updateble skills
            foreach (var skill in activeSkills.Where(x => x.isActive && x.isUpdatable))
            {
                skill.OnGameUpdate();
            }
        }


        /// <summary>
        /// Get skill by id
        /// </summary>
        /// <param name="id">id</param>
        /// <returns>skill</returns>
        public Skill GetSkill(string id) =>
            activeSkills.First(x => x.skillId == id);

        /// <summary>
        /// Update skill dots ui chain
        /// </summary>
        public void UpdateAllTree()
        {
            foreach (var item in selectableSkills)
            {
                //Debug.Log($"Checking {item.GetSkillInstance().skillId}");
                if (!item)
                    continue;
                item.CheckSelf();
            }
        }
    }
}