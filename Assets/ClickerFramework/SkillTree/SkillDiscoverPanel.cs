﻿using System;
using BreakInfinity;
using ClickerFramework.Extensions;
using ClickerFramework.Multilanguage;
using UnityEngine;
using UnityEngine.UI;

namespace ClickerFramework.SkillTree
{
    /// <summary>
    /// Class for skill buy panel
    /// </summary>
    public class SkillDiscoverPanel : MonoBehaviour
    {
        /// <summary>
        /// Buy button label
        /// </summary>
        [SerializeField] private Text buyButtonText;

        /// <summary>
        /// Skill destiption
        /// </summary>
        [SerializeField] private Text descriptionText;

        /// <summary>
        /// Buy button
        /// </summary>
        [SerializeField] private Button buyButton;

        private BigDouble price;

        /// <summary>
        /// Setup panel for selected skill
        /// </summary>
        /// <param name="sender">UI skill dot</param>
        /// <param name="skill">skill-source</param>
        public void SetPanel(SkillSelectable sender, Skill skill)
        {
            gameObject.SetActive(true);
            descriptionText.text = LanguageLoader.Instance.GetEntryValue(skill.description);
            if (!skill.isActive)
            {
                buyButton.interactable = true;
                buyButtonText.text = NumberHelper.FormatNumber(skill.price);
            }
            else
            {
                buyButton.interactable = false;
                buyButtonText.text = LanguageLoader.Instance.GetEntryValue("{buttonSkillBought}");
                return;
            }

            price = skill.price;
            buyButton.onClick.RemoveAllListeners();

            //creation method for purchase
            buyButton.onClick.AddListener(() =>
            {
                if (!sender.canWeBuyIt) return;
                if (World.Instance.CurrentCrystals < price)
                {
                    Menu.Instance.SetNotEnoughCrystalsWindowState(true);
                    return;
                }
                AudioManager.Instance.PlaySound("upgrade_skill");
                World.Instance.CurrentCrystals -= price;
                buyButton.interactable = false;
                buyButtonText.text = LanguageLoader.Instance.GetEntryValue("{buttonSkillBought}");
                Debug.Log($"{skill.skillId} sold");
                skill.itSold = true;
                sender.Discover();
            });
        }
    }
}