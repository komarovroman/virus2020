﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ClickerFramework
{
    public class AudioManager: MonoBehaviour
    {
        public static AudioManager Instance;
        [SerializeField] private AudioClip bgMusicClip; 
        [SerializeField] private List<AudioObject> audioObjects;
        private AudioSource _musicSource, _firstSoundsSource, _secondSoundsSource; 
        
        private void Awake()
        {
            Instance = this;
        }

        private void Start()
        {
            var sources = GetComponents<AudioSource>();
            _musicSource = sources[0];
            _firstSoundsSource = sources[1];
            _secondSoundsSource = sources[2];

            _musicSource.clip = bgMusicClip;
            _musicSource.loop = true;
            _musicSource.Play();
        }

        public void PlaySound(string name) => _firstSoundsSource.PlayOneShot(audioObjects.FirstOrDefault(x => x.Name == name)?.Clip);
        public void PlaySecondSound(string name) => _secondSoundsSource.PlayOneShot(audioObjects.FirstOrDefault(x => x.Name == name)?.Clip);
    }

    [System.Serializable]
    internal class AudioObject 
    {
        public string Name;
        public AudioClip Clip;
    }
    
}