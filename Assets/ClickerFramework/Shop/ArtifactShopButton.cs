﻿using System;
using System.Collections.Generic;
using BreakInfinity;
using ClickerFramework.Abstract;
using ClickerFramework.Artifacts;
using ClickerFramework.Extensions;
using ClickerFramework.Multilanguage;
using Facebook.Unity;
using Game;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ClickerFramework.Shop
{
    public class ArtifactShopButton : MonoBehaviour
    {
        [SerializeField] internal string artifactName;
        [SerializeField] internal GameObject buttonObject;
        [SerializeField] internal GameObject warning;
        [SerializeField] internal Text buttonText;
        [SerializeField] internal Text nameText;
        [SerializeField] internal Text descriptionText;
        [SerializeField] internal Text typeText;
        
        private IArtifact _artifact;

        private void Start()
        {
            _artifact = ArtifactManager.Instance.GetArtifact(artifactName);
            _artifact.Init();
            
            nameText.text = _artifact.Name;
            descriptionText.text = _artifact.Description;
            typeText.text = "{artifactsType}: " + Enum.GetName(typeof(ArtifactType), _artifact.Type);
            
            _artifact.OnActivate.AddListener(() =>
            {
                buttonObject.SetActive(false);
                this.enabled = false;
            });
            
            _artifact.OnNewPartAdded.AddListener((count) =>
            {
                var parts = count - World.Instance.Save.GetArtifactPartCount(artifactName);
                buttonText.text = parts.ToString();
                warning.SetActive(true);
                if (parts <= 0)
                {
                    _artifact.Activate();
                    buttonObject.SetActive(false);
                }
                
                var icon = transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<Image>().sprite;
                ArtifactNotificationWindow.Instance.Show(artifactName, icon, parts <= 0);
                
                Telemetry.SendEvent(Telemetry.ArtifactCollected, new Dictionary<string, object>()
                {
                    [TelemetryEventParams.name] = LanguageLoader.Instance.GetEntryValue( _artifact.Name),
                    [TelemetryEventParams.count] = parts
                });
                
                Debug.Log($"You get {_artifact.Name}, {parts} left");
            });
            buttonText.text = (_artifact.Parts - World.Instance.Save.GetArtifactPartCount(artifactName)).ToString();
        }
        
    }
}