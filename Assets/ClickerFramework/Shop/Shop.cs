﻿using System;
using System.Collections.Generic;
using System.Linq;
using BreakInfinity;
using UnityEngine;

namespace ClickerFramework.Shop
{
    /// <summary>
    /// Helper class for shops
    /// </summary>
    public class Shop : MonoBehaviour
    {
        private static Dictionary<int, BigDouble> _playerLevelUps = new Dictionary<int, BigDouble>();

        private static BigDouble _lastPlayerPrice;
        //public static Shop Instance;

        private void Start()
        {
            //Setup player level limits (level, cost)
            _playerLevelUps.Add(10, 100);
            _playerLevelUps.Add(25, 1000);
            _playerLevelUps.Add(50, 10000);
            _playerLevelUps.Add(75, 50000);
            _playerLevelUps.Add(100, 500000);
            _playerLevelUps.Add(150, 1000000);
            _playerLevelUps.Add(200, 5000000);
            _playerLevelUps.Add(300, 10000000);
            _playerLevelUps.Add(400, 50000000);
            _playerLevelUps.Add(500, 100000000);
        }

        private void Update()
        {
        }

        /// <summary>
        /// Upgrades player with shop-button
        /// </summary>
        /// <param name="sender">Shop button that caused it</param>
        public static void UpgradePlayer(PlayerShopButton sender)
        {
            World.Instance.CurrentMoney -= sender.price;
                Player.Instance.level++;
            sender.Level = Player.Instance.level;
            Player.Instance.CalculatePlayerDamage();
            sender.damage = Player.Instance.GetPlayerDamage();
            if (IsBigUpgradeAvailable(sender))
            {
                Player.Instance.OnPlayerLevelUp.Invoke();
                AudioManager.Instance.PlaySound("upgrade_tool");
                return;
            }

            if (_lastPlayerPrice > 0)
            {
                sender.price = _lastPlayerPrice * (Player.Instance.minUpgradePrice*1.15f);
                _lastPlayerPrice = -1;
            }
            else
            {
                sender.price *= Player.Instance.minUpgradePrice;
            }
            
            Player.Instance.OnPlayerLevelUp.Invoke();
            AudioManager.Instance.PlaySound("upgrade_tool");
        }
        
        public static void UpgradePlayerSilent(PlayerShopButton sender)
        {
            
            Player.Instance.CalculatePlayerDamage(sender.Level);
            sender.damage = Player.Instance.GetPlayerDamage();
            if (UpgradePlayerLevels(sender))
            {
                /*Player.Instance.CalculatePlayerDamage(sender.Level);
                sender.damage = Player.Instance.GetPlayerDamage();*/
                return;
            }

            if (_lastPlayerPrice > 0)
            {
                sender.price = _lastPlayerPrice * (Player.Instance.minUpgradePrice*1.15f);
                _lastPlayerPrice = -1;
            }
            else
            {
                if (sender.Level<49 && sender.Level<Player.Instance.level || sender.Level>50 && sender.Level<=Player.Instance.level)
                    sender.price *= Player.Instance.minUpgradePrice;
            }
        }

        /// <summary>
        /// Checks for limit upgrade
        /// </summary>
        /// <param name="sender">Shop button that caused it</param>
        /// <returns>is available</returns>
        public static bool IsBigUpgradeAvailable(PlayerShopButton sender)
        {
            if (sender.Level==0 || sender.Level%50!=0) return false;

            _lastPlayerPrice = sender.price;
            sender.price = _lastPlayerPrice * 10;
            World.Instance.playerDamageGrowth *= 2f;

            return true;
        }
        
        public static bool UpgradePlayerLevels(PlayerShopButton sender)
        {
            
            if (sender.Level==0 || (sender.Level)%50!=0) return false;

            _lastPlayerPrice = sender.price;
            sender.price = _lastPlayerPrice * 10;
            World.Instance.playerDamageGrowth *= 2f;
            
            return true;
            
        }

        /// <summary>
        /// Buy/Upgrades tool from shop-button and name
        /// </summary>
        /// <param name="sender">Shop button that caused it</param>
        /// <param name="attackerName">tool name</param>
        public static void BuyAttacker(AttackerShopButton sender, string attackerName)
        {
            World.Instance.CurrentMoney -= sender.price;
            var attacker = Player.Instance.GetAttacker(attackerName);
            attacker.Level = World.Instance.Save.GetToolLevel(attackerName);
            attacker.LevelUp();
            //Debug.Log($"{attacker.Name} {attacker.Level}");
            sender.level = attacker.Level;

            //skips level that needed special cost (limits)
            if (sender.level - 1 % 50 == 0 && sender.level - 1 > 0)
            {
                sender.price *= attacker.Price * 10;
            }
            else
            {
                sender.price = attacker.Price;
            }

            sender.damage = attacker.Damage;
            World.Instance.Save.SetToolLevel(attackerName, attacker.Level);
            if (!attacker.isActive) attacker.isActive = true;
        }

        /// <summary>
        /// Check if it tool can be upgraded or used
        /// </summary>
        /// <param name="attackerName">tool name</param>
        /// <returns>can it be used?</returns>
        public static bool IsAttackerCanBeUpgraded(string attackerName) =>
            !Player.Instance.GetAttacker(attackerName).isInCompany;
    }
}