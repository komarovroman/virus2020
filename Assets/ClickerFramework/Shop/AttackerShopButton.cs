﻿using System;
using System.Collections.Generic;
using BreakInfinity;
using ClickerFramework.Extensions;
using ClickerFramework.Multilanguage;
using Facebook.Unity;
using Game;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ClickerFramework.Shop
{
    public class AttackerShopButton : MonoBehaviour
    {
        internal BigDouble price;
        internal BigDouble damage;
        internal int level;
        private bool _inCompany;
        
        [SerializeField] internal Button buyButton;
        [SerializeField] internal Text buyButtonText;
        [SerializeField] internal Text buyLevelText;
        [SerializeField] internal Text buyNameText;
        [SerializeField] internal Text buyDamageText;
        [SerializeField] internal Image levelFill;
        [SerializeField] internal Image buttonHint;
        [SerializeField] public string attackerName;
        
        private PassiveAttacker _attacker; 
        
        
        private void Start()
        {
            _attacker = Player.Instance.GetAttacker(attackerName);
            
            buyButton.onClick.AddListener(() =>
            {
                Shop.BuyAttacker(this, attackerName);
                Player.Instance.CalculateTotalAttackersDamage();
                AudioManager.Instance.PlaySound("upgrade_tool");
                
                Telemetry.SendEvent(Telemetry.ToolUpgrade, new Dictionary<string, object>()
                {
                    [TelemetryEventParams.name] = LanguageLoader.Instance.GetEntryValue("{"+attackerName+"}"),
                    [TelemetryEventParams.count] = _attacker.Level
                });
                
                SetUp();
            });
            
            _attacker.OnCompanyStateChanged.AddListener((state) =>
            {
                _inCompany = state;
                Player.Instance.CalculateTotalAttackersDamage();
            });
            
            buyNameText.text = "{"+_attacker.Name+"}";
            _attacker.Level = World.Instance.Save.GetToolLevel(attackerName);

            if (_attacker.Level % 50 == 0 && _attacker.Level > 0)
            {
                price *= _attacker.Price * 10;
            }
            else
            {
                price = _attacker.Price;
            }

            damage = _attacker.Damage;
            level = _attacker.Level;
            if (level > 0)
                _attacker.isActive = true;

            SetUp();
        }

        private void SetUp()
        {
            buyButtonText.text = NumberHelper.FormatNumber(price);

            buyDamageText.text =
                LanguageLoader.Instance.GetEntryValue("{currentToolDamage}") +
                NumberHelper.FormatNumber(_attacker.PrevDamage) + '\n' +
                LanguageLoader.Instance.GetEntryValue("{nextToolDamage}") +
                NumberHelper.FormatNumber(damage);
            buyLevelText.text = "Lv " + NumberHelper.FormatNumber(level);
            levelFill.fillAmount = (1f / 100f) * level;
        }

        private void LateUpdate()
        {
            buyButton.interactable =  !_inCompany && World.Instance.CurrentMoney >= price;
        }
    }
}