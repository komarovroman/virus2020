using System.Collections.Generic;
using ClickerFramework.Heros;
using ClickerFramework.Multilanguage;
using Facebook.Unity;
using Game;
using UnityEngine;
using UnityEngine.UI;

namespace ClickerFramework.Shop
{
    public class HeroShopButton : MonoBehaviour
    {
        [SerializeField] private Hero hero;
        [SerializeField] internal Image icon;
        [SerializeField] internal Text heroName;
        [SerializeField] internal Button buyButton;
        [SerializeField] internal Text buyButtonLabel;
        [SerializeField] internal Text description;
        
        private void Start()
        {
            heroName.text = hero.heroName;
            buyButtonLabel.text = hero.price.ToString();
            description.text = hero.description;
            
            if (hero.isActive)
            {
                hero.Activate();
                buyButton.gameObject.SetActive(false);
            }else buyButton.onClick.AddListener(() =>
            {
                if (World.Instance.CurrentCrystals < hero.price)
                {
                    Menu.Instance.SetNotEnoughCrystalsWindowState(true);
                    return;
                }
                
                Telemetry.SendEvent(Telemetry.HeroBuy, new Dictionary<string, object>()
                {
                    [TelemetryEventParams.name] =  LanguageLoader.Instance.GetEntryValue( hero.heroName)
                });
                
                AudioManager.Instance.PlaySound("earn_crystals");
                World.Instance.CurrentCrystals -= hero.price;
                World.Instance.Save.AddNewHero(hero.heroName);
                buyButton.gameObject.SetActive(false);
                hero.Activate();
            });
        }
        
    }
}