﻿using System;
using BreakInfinity;
using ClickerFramework.Extensions;
using ClickerFramework.Multilanguage;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ClickerFramework.Shop
{
    public class PlayerShopButton : MonoBehaviour
    {
        private int _level;

        internal BigDouble price;
        internal BigDouble damage;

        internal int Level
        {
            get => _level;
            set
            {
                _level = value;
                World.Instance.Save.SetPlayerLevel(value);
            }
        }

        [SerializeField] internal Button buyButton;
        [SerializeField] internal Text buyButtonText;
        [SerializeField] internal Text buyLevelText;
        [SerializeField] internal Text buyNameText;
        [SerializeField] internal Text buyDamageText;
        [SerializeField] internal Image levelFill;
        
        
        private void Start()
        {
            buyButton.onClick.AddListener(() => { Shop.UpgradePlayer(this); });
            buyNameText.text = World.Instance.Save.FacebookData.UserName;
            //Level = Player.Instance.level;

            price = 10; //old price was 5
            if (Player.Instance.level > 1)
            {
                
                for (Level = 1; Level < Player.Instance.level+1; Level++)
                {
                    //Debug.Log($"Price {price}");
                    //Player.Instance.CalculatePlayerDamage(Level);

                    Shop.UpgradePlayerSilent(this);
                    Debug.Log($"{Level} {World.Instance.playerDamageGrowth} {Player.Instance.GetPlayerDamage()} {price}");
                    
                }
                //price = 10*BigDouble.Pow(Player.Instance.minUpgradePrice, Player.Instance.level-1);
                
                //price *= BigDouble.Pow(Player.Instance.minUpgradePrice, (n)-1);
            }
            Level = Player.Instance.level;
            //Shop.UpgradePlayerLevels(this);
            /*Player.Instance.CalculatePlayerDamage(Level);
            damage = Player.Instance.GetPlayerDamage();*/
            
            Player.Instance.OnPlayerLevelUp.AddListener(SetUp);
            SetUp();
        }

        private void SetUp()
        {
            buyButtonText.text = NumberHelper.FormatNumber(price);
            
            buyDamageText.text = LanguageLoader.Instance.GetEntryValue("{currentToolDamage}") +
                                 NumberHelper.FormatNumber(damage) + '\n' +
                                 LanguageLoader.Instance.GetEntryValue("{nextToolDamage}") +
                                 NumberHelper.FormatNumber(Player.Instance.CalculateNextPlayerDamage());
            
            buyLevelText.text = "Lv " + NumberHelper.FormatNumber(Level);
            levelFill.fillAmount = (1f / 1000f) * Level;
            World.Instance.SetActiveDamage(damage);
        }

        private void LateUpdate()
        {
            buyButton.interactable = World.Instance.CurrentMoney >= price;
        }
    }
}