﻿using System;
using BreakInfinity;
using ClickerFramework.Abstract;
using ClickerFramework.Extensions;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace ClickerFramework
{
    [CreateAssetMenu(fileName = "Attacker", menuName = "Create Attacker", order = 0)]
    public class PassiveAttacker : ScriptableObject, IClickerAsset
    {
        public string Name;
        [NonSerialized] private int level = 1;
        [SerializeField] private BigDouble baseDamage;
        [SerializeField] private BigDouble basePrice;
        [SerializeField] private float damageMultiplier, priceMultiplier;
        public Sprite icon;


        public float baseInterval;
        public bool isActive = false;
        [NonSerialized] private bool _isInCompany = false;

        [HideInInspector] public float Interval;
        public BigDouble Damage => ((level + 1) * baseDamage);
        public BigDouble PrevDamage
        {
            get
            {
                int prev = level - 1;
                if (prev < 0)
                    return 0;
                else
                {
                    return ((prev+1) * baseDamage);
                }
            }
        }

        public BigDouble Price
        {
            get
            {
                /*var tmp = basePrice;
                for (var i = 0; i < level + 1; i++)
                {
                    tmp *= priceMultiplier;
                }
                */

                return BigDouble.Round(NumberHelper.BigGeomProgression(basePrice, priceMultiplier, level));
            }
        }

        public int Level
        {
            get => level;
            set => level = value;
        }

        public bool isInCompany
        {
            //get => _isInCompany;
            get => false;
            
            set
            {
                //_isInCompany = value; 
                //Debug.Log($"{Name}: {_isInCompany}");
                //OnCompanyStateChanged.Invoke(_isInCompany);
            }
        }

        public ClickerBoolEvent OnCompanyStateChanged = new ClickerBoolEvent();
        
        public void LevelUp() => level++;
    }
}