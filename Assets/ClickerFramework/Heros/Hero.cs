using System;
using ClickerFramework.Abstract;
using UnityEngine;
using UnityEngine.Serialization;

namespace ClickerFramework.Heros
{
    [CreateAssetMenu(fileName = "Hero", menuName = "Create new Hero", order = 0)]
    public class Hero: ScriptableObject, IClickerAsset
    {
        public Sprite icon;
        public string heroName;
        public string description;
        public int price;
        public float delay;
        public int weight;
        
        public bool isActive
        {
            get => World.Instance.Save.ContainsHero(heroName);
        }

        /// <summary>
        /// Possible multipliers of hero
        /// </summary>
        [SerializeField] [Range(0, 1)] private float moneyMultiplier,
            activeDamageMultiplier,
            passiveDamageMultiplier,
            timeDisMultiplier,
            rewardMultiplier;

        /// <summary>
        /// Activate and setup multipliers
        /// </summary>
        public void Activate()
        {
            Debug.Log($"{heroName} activated");
            World.Instance.Save.GameStatistic.AnyHeroBought = true;
            
            World.Instance.GlobalDamageMultiplier += activeDamageMultiplier;
            World.Instance.GlobalPassiveDamageMultiplier += passiveDamageMultiplier;
            World.Instance.GlobalMoneyMultiplier += moneyMultiplier;
            World.Instance.GlobalTravelTimeMultiplier -= timeDisMultiplier;
            World.Instance.GlobalTravelRewardsMultiplier += rewardMultiplier;
            
            TravelController.Instance.AssignHero(this);
        }
    }
}