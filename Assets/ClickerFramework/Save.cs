﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using AppsFlyerSDK;
using BreakInfinity;
using ClickerFramework.Extensions;
using ClickerFramework.Multilanguage;
using Facebook.Unity;
using Game;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace ClickerFramework
{
    /// <summary>
    /// Save data class
    /// </summary>
    public class Save: MonoBehaviour
    {

        /// <summary>
        /// Save data
        /// </summary>
        private SaveFile _saveFile;

        public static Dictionary<int, string> Countries = new Dictionary<int, string>
        {
            //[1] = "Canada",
            [12] = "Canada",
            [18] = "USA",
            [21] = "Mexico",
            [26] = "Brazil",
            [29] = "Argentina",
            [30] = "South Africa",
            [31] = "Egypt",
            [32] = "Turkey",
            [33] = "Belarus",
            [34] = "Italy",
            [35] = "France",
            [36] = "Spain",
            [45] = "Greenland",
            [77] = "Russia",
            [78] = "Japan",
            [80] = "Japan",
            [88] = "China",
            [90] = "Kazakhstan",
            [93] = "India",
            [97] = "Indonesia",
            [104] = "Australia"
        };
        
        public static Dictionary<int, string> Continents = new Dictionary<int, string>
        {
            //[1] = "North_America",
            [21] = "North_America",
            [29] = "South_America",
            [31] = "Africa",
            [77] = "Europe",
            [97] = "Asia",
            [104] = "Australia",
        };
        
        private float updatetimer = 10f;
        
        public void Awake()
        {
            Init();
        }

        /// <summary>
        /// Save everything to binary format
        /// </summary>
        internal void SaveAll()
        {
            StartCoroutine(SaveAllAsync());
        }

        internal void SaveAllNow()
        {
            var binary = new BinaryFormatter();
            string data;
            using (var stream = new MemoryStream())
            {
                binary.Serialize(stream, _saveFile);
                stream.Position = 0;
                data = Convert.ToBase64String(stream.ToArray());
            }
            
            //Debug.Log("Saving "+data);
            PlayerPrefs.SetString("gamedata", data);
            PlayerPrefs.Save();
        }
        
        public void Init()
        {
            
            _saveFile = new SaveFile();
            Load();
            _saveFile.gameStatistics.ResetEvents();
        }
        
        private IEnumerator SaveAllAsync()
        {
            var binary = new BinaryFormatter();
            string data;
            using (var stream = new MemoryStream())
            {
                binary.Serialize(stream, _saveFile);
                stream.Position = 0;
                data = Convert.ToBase64String(stream.ToArray());
            }

            yield return new WaitForFixedUpdate();
            //Debug.Log("Saving "+data);
            PlayerPrefs.SetString("gamedata", data);
            PlayerPrefs.Save();
        }

        private void OnApplicationQuit()
        {
            SaveAll();
        }

        private void OnApplicationFocus(bool hasFocus)
        {
            if (!hasFocus) SaveAll();
        }

        private void OnApplicationPause(bool pauseStatus)
        {
            if (pauseStatus) SaveAll();
        }

        public void LateUpdate()
        {
            updatetimer -= Time.fixedDeltaTime;
            if (!(updatetimer < 0)) return;
            
            SaveAll();
            updatetimer = 10;
        }

        /// <summary>
        /// Load data from binary file
        /// </summary>
        private void Load()
        {
            var binary = new BinaryFormatter();

            string data;
            if (PlayerPrefs.HasKey("gamedata"))
                data = PlayerPrefs.GetString("gamedata");
            else
            {
                _saveFile = new SaveFile();
                return;
            }

            using (var stream = new MemoryStream(Convert.FromBase64String(data)))
            {
                _saveFile = (SaveFile) binary.Deserialize(stream);
            }
        }

        /// <summary>
        /// Resetting settings 
        /// </summary>
        public void RestoreDefaults()
        {
            _saveFile = new SaveFile();
            PlayerPrefs.DeleteAll();
            SaveAll();
        }

        public TimeSpan GetTime() => _saveFile.LastTime;
        public TimeSpan GetTravelEndTime() => _saveFile.TravelEndTime;
        public TimeSpan GetHeroUpdateTime() => _saveFile.HeroUpdateTime;
        public void SetHeroUpdateTime(TimeSpan newTime)
        {
            _saveFile.HeroUpdateTime = newTime;
            SaveAll();
        }

        public int GetReachedLevel() => _saveFile.ReachedLevel;
        public int GetPlayerLevel() => _saveFile.PlayerLevel;
        public int GetTravelLevel() => _saveFile.TravelLevel;

        public int GetToolLevel(string toolName)
        {
            /*if (_saveFile.ToolLevels.ContainsKey(toolName))
                Debug.Log($"{toolName}: {_saveFile.ToolLevels[toolName]}");*/
            return _saveFile.ToolLevels.ContainsKey(toolName) ? _saveFile.ToolLevels[toolName] : 0;
        }

        public int GetArtifactPartCount(string artifactName) => _saveFile.ArtifactParts.ContainsKey(artifactName)
            ? _saveFile.ArtifactParts[artifactName]
            : 0;

        public BigDouble GetMoney() => _saveFile.Money;
        public BigDouble GetCrystals() => _saveFile.Crystals;

        public bool IsSkillBought(string skillName) =>
            _saveFile.SkillTreePoints.Contains(skillName);

        public void SetReachedLevel(int newLevel)
        {
            if (_saveFile.ReachedLevel >= newLevel) return;
            _saveFile.ReachedLevel = newLevel;
            var country = Countries.FirstOrDefault(
                c => NumberHelper.LoopNumber(newLevel, 104) <= c.Key).Value;
            
            Telemetry.SendEvent(Telemetry.LevelComplete,  new Dictionary<string, object>()
            {
                [TelemetryEventParams.name] = country,
                [TelemetryEventParams.count] = newLevel
            }); 
            SaveAll();
        }

        public int CompletedCountriesNum() => _saveFile.CompletedCountries.Count;
        public bool IsCountryCompleted(string country) => _saveFile.CompletedCountries.Contains(country);
        public void SetCountryAsCompleted(string country) => _saveFile.CompletedCountries.Add(country);
        public void ClearCompletedCountries()
        {
            _saveFile.CompletedCountries.Clear();
            SaveAll();
        }
        
        public int CompletedContinentsNum() => _saveFile.CompletedContinents.Count;
        public bool IsContinentCompleted(string continent) => _saveFile.CompletedContinents.Contains(continent);
        public void SetContinentAsCompleted(string continent) => _saveFile.CompletedContinents.Add(continent);
        public void ClearCompletedContinent()
        {
            _saveFile.CompletedContinents.Clear();
            SaveAll();
        }

        public void IncTravelLevel()
        {
            _saveFile.TravelLevel++;
            SaveAll();
        }

        public void SetPlayerLevel(int newLevel)
        {
            _saveFile.PlayerLevel = newLevel;
            SaveAll();
        }

        public void SetToolLevel(string toolName, int newLevel)
        {
            if (_saveFile.ToolLevels.ContainsKey(toolName))
                _saveFile.ToolLevels[toolName] = newLevel;
            else
                _saveFile.ToolLevels.Add(toolName, newLevel);
            SaveAll();
        }

        public void SetArtifactPartCount(string artifactName, int count)
        {
            if (_saveFile.ArtifactParts.ContainsKey(artifactName))
                _saveFile.ArtifactParts[artifactName] = count;
            else
                _saveFile.ArtifactParts.Add(artifactName, count);
            SaveAll();
        }

        public void AddNewSkill(string skillName)
        {
            _saveFile.SkillTreePoints.Add(skillName);
            SaveAll();
        }

        public void SetTime(TimeSpan newTime)
        {
            _saveFile.LastTime = newTime;
            SaveAll();
        }

        public void SetTravelEndTime(TimeSpan newTime)
        {
            _saveFile.TravelEndTime = newTime;
            SaveAll();
        }

        public void AddToolToTravel(string name)
        {
            _saveFile.ToolsInTravel.Add(name);
            SaveAll();
        }

        public void AddCompletedTask(string name) => _saveFile.CompletedTasks.Add(name);
        public bool ContainsTask(string name) => _saveFile.CompletedTasks.Contains(name);
        
        public void AddNewHero(string name) => _saveFile.HeroesList.Add(name);
        public bool ContainsHero(string name) => _saveFile.HeroesList.Contains(name);

        
        public void ClearToolsInTravel()
        {
            _saveFile.ToolsInTravel.Clear();
            SaveAll();
        }

        public int GetAvatarNum() => _saveFile.AvatarNum;
        public void SetAvatarNum(int num) => _saveFile.AvatarNum = num;
        
        public List<string> GetToolsInTravel() => _saveFile.ToolsInTravel;
        
        public void SetLanguage(int lang) => _saveFile.Language = lang;
        public Languages GetLanguage() => (Languages) _saveFile.Language;

        public void SetMoney(BigDouble money)
        {
            _saveFile.Money = money;
            SaveAll();
        }

        public void SetCrystals(BigDouble crystals)
        {
            _saveFile.Crystals = crystals;
            SaveAll();
        }

        public bool IsFirstTime() => _saveFile.FirstTime;

        public void GameStartedFirstTime()
        {
            _saveFile.FirstTime = false;
            SaveAll();
        }
        
        public ref GameStatistics GameStatistic => ref _saveFile.gameStatistics;
        public ref FacebookData FacebookData => ref _saveFile.facebookData;
    }


    /// <summary>
    /// Class for serialization
    /// </summary>
    [System.Serializable]
    internal class SaveFile
    {
        public bool FirstTime = true;
        
        public int AvatarNum = -9999;
        public TimeSpan LastTime = TimeSpan.Zero;
        public TimeSpan TravelEndTime = TimeSpan.Zero;
        public TimeSpan HeroUpdateTime = TimeSpan.Zero;
        public int ReachedLevel = 1;
        public int PlayerLevel = 1;
        public BigDouble Money = 0;
        public BigDouble Crystals = 0;
        public int TravelLevel = 1;
        public int Language = -1;

        public readonly Dictionary<string, int> ToolLevels = new Dictionary<string, int>(32);
        public readonly Dictionary<string, int> ArtifactParts = new Dictionary<string, int>(13);
        public readonly List<string> CompletedTasks = new List<string>();
        public readonly List<string> SkillTreePoints = new List<string>();
        public readonly List<string> ToolsInTravel = new List<string>();
        public readonly List<string> HeroesList = new List<string>();
        public readonly List<string> CompletedCountries = new List<string>();
        public readonly List<string> CompletedContinents = new List<string>();
        
        public GameStatistics gameStatistics;
        public FacebookData facebookData; 

        public SaveFile()
        {
            gameStatistics = new GameStatistics();
            facebookData = new FacebookData();
        }
    }

    /// <summary>
    /// Class that contains all tasks helping statistics
    /// </summary>
    [System.Serializable]
    public class GameStatistics
    {
        [NonSerialized] public UnityEvent OnKilledMonstersChanged;
        [NonSerialized] public UnityEvent OnKilledBossesChanged;
        [NonSerialized] public UnityEvent OnTotalMoneyChanged;
        [NonSerialized] public UnityEvent OnClearedFirstPartOfCountries;
        [NonSerialized] public UnityEvent OnClearedSecondPartOfCountries;
        [NonSerialized] public UnityEvent OnClearedThirdPartOfCountries;
        [NonSerialized] public UnityEvent OnAnyHeroBought;
        [NonSerialized] public UnityEvent OnArtifactCollected;
        [NonSerialized] public UnityEvent OnDonated;


        private int _killedMonsters;
        private int _killedBosses;
        private int _artifactCount;
        private BigDouble _totalMoney;
        private bool _clearedFirstPartOfCountries;
        private bool _clearedSecondPartOfCountries;
        private bool _clearedThirdPartOfCountries;
        private bool _anyHeroBought;
        private bool _donated;

        /// <summary>
        /// Reset all events after serialization
        /// </summary>
        internal void ResetEvents()
        {
            OnDonated = new UnityEvent();
            OnArtifactCollected = new UnityEvent();
            OnAnyHeroBought = new UnityEvent();
            OnKilledBossesChanged = new UnityEvent();
            OnKilledMonstersChanged = new UnityEvent();
            OnKilledMonstersChanged = new UnityEvent();
            OnTotalMoneyChanged = new UnityEvent();
            OnClearedFirstPartOfCountries = new UnityEvent();
            OnClearedSecondPartOfCountries = new UnityEvent();
            OnClearedThirdPartOfCountries = new UnityEvent();
        }


        public int KilledMonsters
        {
            get => _killedMonsters;
            set
            {
                //Debug.Log("Set new val");
                _killedMonsters = value;
                OnKilledMonstersChanged.Invoke();
            }
        }

        public int KilledBosses
        {
            get => _killedBosses;
            set
            {
                _killedBosses = value;
                OnKilledBossesChanged.Invoke();
            }
        }

        public BigDouble TotalMoney
        {
            get => _totalMoney;
            set
            {
                _totalMoney = value;
                OnTotalMoneyChanged.Invoke();
            }
        }

        public bool ClearedFirstPartOfCountries
        {
            get => _clearedFirstPartOfCountries;
            set
            {
                _clearedFirstPartOfCountries = value;
                OnClearedFirstPartOfCountries.Invoke();
            }
        }

        public bool ClearedSecondPartOfCountries
        {
            get => _clearedSecondPartOfCountries;
            set
            {
                _clearedSecondPartOfCountries = value;
                OnClearedSecondPartOfCountries.Invoke();
            }
        }

        public bool ClearedThirdPartOfCountries
        {
            get => _clearedThirdPartOfCountries;
            set
            {
                _clearedThirdPartOfCountries = value;
                OnClearedThirdPartOfCountries.Invoke();
            }
        }

        public bool AnyHeroBought
        {
            get => _anyHeroBought;
            set
            {
                _anyHeroBought = value;
                OnAnyHeroBought.Invoke();
            }
        }


        public bool Donated
        {
            get => _donated;
            set
            {
                _donated = value;
                OnDonated.Invoke();
            }
        }

        public int ArtifactCount
        {
            get => _artifactCount;
            set
            {
                _artifactCount = value;
                OnArtifactCollected.Invoke();
            }
        }
    }

    [System.Serializable]
    public class FacebookData
    {
        public bool Logged = false;
        public string UserName = "Player";
        public byte[] ProfilePicture = null;
    }
}