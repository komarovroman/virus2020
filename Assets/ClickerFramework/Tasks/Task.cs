using System;
using ClickerFramework.Abstract;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace ClickerFramework.Tasks
{
    /// <summary>
    /// Base class for task
    /// </summary>
    public abstract class Task : ScriptableObject, IClickerAsset
    {
        /// <summary>
        /// Reward from task
        /// </summary>
        public int reward;

        /// <summary>
        /// Task description
        /// </summary>
        public string description;

        /// <summary>
        /// State: is completed and reward can be claimed?
        /// </summary>
        public bool Completed { get; protected set; } = false;

        /// <summary>
        /// State: is completed and claimed
        /// </summary>
        public bool Claimed { get; protected set; } = false;

        /// <summary>
        /// Event for TaskItem class
        /// </summary>
        internal UnityEvent onCompleted;

        public void Init()
        {
            Claimed = Completed = false;
            onCompleted = new UnityEvent();
            SetupListeners();
            if (World.Instance.Save.ContainsTask(this.name))
                Claimed = Completed = true;
            else
            {
                Check();
            }
        }


        /// <summary>
        /// Method for setup events from GameStatistics
        /// </summary>
        protected abstract void SetupListeners();

        /// <summary>
        /// Method that check progress of task
        /// </summary>
        protected abstract void Check();
    }
}