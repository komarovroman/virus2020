using System;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace ClickerFramework.Tasks
{
    public class TaskItem : MonoBehaviour
    {
        /// <summary>
        /// Task
        /// </summary>
        [SerializeField] private Task task;

        /// <summary>
        /// Description text object 
        /// </summary>
        [SerializeField] private Text description;

        /// <summary>
        /// Reward text
        /// </summary>
        [SerializeField] private Text reward;

        /// <summary>
        /// Icon for crystals
        /// </summary>
        [SerializeField] private Image icon;

        /// <summary>
        /// First label uncompleted task
        /// </summary>
        [SerializeField] private Text buttonLabel;

        /// <summary>
        /// Second label for completed task
        /// </summary>
        [SerializeField] private Text buttonLabel2;

        /// <summary>
        /// Claim button
        /// </summary>
        [SerializeField] private Button claimButton;


        private void Start()
        {
            task.Init();
            reward.text = task.reward.ToString();
            description.text = task.description;

            //TODO: Create method for setting complete/uncomplete ui button style
            var claimed = task.Completed && task.Claimed;
            claimButton.interactable = claimed;
            if (claimed)
            {
                icon.gameObject.SetActive(false);
                reward.gameObject.SetActive(false);
                buttonLabel.gameObject.SetActive(false);
                buttonLabel2.gameObject.SetActive(true);
                claimButton.interactable = false;
                Destroy(this);
            }
            else
                buttonLabel2.gameObject.SetActive(false);

                task.onCompleted.AddListener(() =>
                {
                    claimButton.interactable = true;
                    claimButton.onClick.AddListener(() =>
                    {
                        AudioManager.Instance.PlaySound("earn_crystals");
                        World.Instance.AddCrystals(task.reward);
                        icon.gameObject.SetActive(false);
                        reward.gameObject.SetActive(false);
                        buttonLabel.gameObject.SetActive(false);
                        buttonLabel2.gameObject.SetActive(true);
                        World.Instance.Save.AddCompletedTask(task.name);
                        claimButton.interactable = false;
                        
                    });
                    task.onCompleted.RemoveAllListeners();
                    this.enabled = false;
                });
        }
    }
}