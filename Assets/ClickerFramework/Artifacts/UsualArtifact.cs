﻿using ClickerFramework.Abstract;
using ClickerFramework.Extensions;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace ClickerFramework.Artifacts
{
    /// <summary>
    /// Artifact class
    /// </summary>
    [CreateAssetMenu(fileName = "TestArtifact", menuName = "Create TestArtifact", order = 0)]
    public class UsualArtifact : ScriptableObject, IArtifact, IClickerAsset
    {
        /// <summary>
        /// Artifact name
        /// </summary>
        [SerializeField] private new string name;

        /// <summary>
        /// Description
        /// </summary>
        [SerializeField] [Multiline] private string description;

        /// <summary>
        /// Rarity type
        /// </summary>
        [SerializeField] private ArtifactType type;

        /// <summary>
        /// Needed parts
        /// </summary>
        [SerializeField] private int parts;

        /// <summary>
        /// Is collected and active?
        /// </summary>
        [SerializeField] private bool active;

        /// <summary>
        /// Possible multipliers of artifact
        /// </summary>
        [SerializeField] [Range(0, 1)] private float moneyMultiplier, activeDamageMultiplier, passiveDamageMultiplier;

        public string Name => name;
        public string Description => description;
        public ArtifactType Type => type;

        public int Parts
        {
            get => parts;
            set
            {
                parts = value;
                OnNewPartAdded.Invoke(parts);
            }
        }

        public bool IsActive
        {
            get => active;
            //set => active = value;
        }

        public UnityEvent OnActivate { get; private set; }
        public ClickerIntEvent OnNewPartAdded { get; private set; }

        public void Init()
        {
            OnActivate = new UnityEvent();
            OnNewPartAdded = new ClickerIntEvent();
        }

        /// <summary>
        /// Activate and setup multipliers
        /// </summary>
        public void Activate()
        {
            World.Instance.GlobalDamageMultiplier += activeDamageMultiplier;
            World.Instance.GlobalPassiveDamageMultiplier += passiveDamageMultiplier;
            World.Instance.GlobalMoneyMultiplier += moneyMultiplier;

            World.Instance.Save.GameStatistic.ArtifactCount++;
            //World.Instance.Save.SaveAll();

            active = true;
            OnActivate.Invoke();
        }
    }
}