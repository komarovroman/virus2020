﻿using System;
using System.Collections.Generic;
using System.Linq;
using ClickerFramework.Abstract;
using ClickerFramework.Extensions;
using ClickerFramework.Multilanguage;
using Facebook.Unity;
using Game;
using UnityEngine;
using Random = UnityEngine.Random;

namespace ClickerFramework.Artifacts
{
    /// <summary>
    /// Manager for artifacts
    /// </summary>
    public class ArtifactManager : MonoBehaviour
    {
        public static ArtifactManager Instance;

        
        /// <summary>
        /// List of available artifacts
        /// </summary>
        [SerializeField] private UsualArtifact[] allArtifacts;

        private void Awake()
        {
            Instance = this;
            Debug.Log("Started");
        }

        private void Update()
        {
        }

        /// <summary>
        /// Calculates what kind of rarity will drop to player
        /// </summary>
        public void GenerateNextArtifact()
        {
            var num = Random.Range(1, 101);
            if (num >= 1 && num <= 46)
                Up(ArtifactType.Common);
            else if (num >= 46 && num <= 75)
                Up(ArtifactType.Uncommon);
            else if (num >= 75 && num <= 90)
                Up(ArtifactType.Rare);
            else if (num >= 90 && num <= 100)
                Up(ArtifactType.Legendary);
        }

        /// <summary>
        /// Add piece of artifact or activate it if all parts collected
        /// </summary>
        /// <param name="type">rarity</param>
        private void Up(ArtifactType type)
        {
            var artifacts = allArtifacts.Where(a => a.Type == type).ToArray();
            var artifact = artifacts[Random.Range(0, artifacts.Length)];
            if (!artifact.IsActive)
            {
                World.Instance.Save.SetArtifactPartCount(artifact.Name,
                    World.Instance.Save.GetArtifactPartCount(artifact.Name) + 1);
            }
            
            
            
            artifact.OnNewPartAdded.Invoke(artifact.Parts);
        }

        /// <summary>
        /// Get artifact by name from list
        /// </summary>
        /// <param name="name">name</param>
        /// <returns>artifact</returns>
        public UsualArtifact GetArtifact(string name)
            => allArtifacts.FirstOrDefault(x => x.Name == name);
    }
}