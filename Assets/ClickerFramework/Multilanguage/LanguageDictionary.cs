using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using fastJSON;

namespace ClickerFramework.Multilanguage
{
    [System.Serializable]
    public class LanguageDictionary
    {
        [SerializeField] private List<LanguageDictionaryElement> data;

        public string GetEntry(string identifier, Languages lang)
        {
            var o = data.FirstOrDefault(x => x.identifier == identifier)?.values[(int) lang];
            if (o == null)
                throw new Exception($"LanguageDictionary error: Entry {identifier} not found! Entry count {data.Count}");

            return o;
        }

        public string Deserialize() => JSON.ToNiceJSON(data);

        public void Serialize(string json) =>
            this.data = JSON.ToObject<List<LanguageDictionaryElement>>(json);
    }

    
    [System.Serializable]
    public class LanguageDictionaryElement
    {
        [SerializeField] public string identifier;
        [SerializeField] public List<string> values;
    }
    
}