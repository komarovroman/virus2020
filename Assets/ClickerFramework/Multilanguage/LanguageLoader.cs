using System;
using System.Linq;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ClickerFramework.Multilanguage
{
    
    public enum Languages {English = 1, Russian = 0}
    
    
    public class LanguageLoader: MonoBehaviour
    {
        public UnityEvent onLoaded;
        public static LanguageLoader Instance;
        
        [SerializeField] private LanguageData dataObject;
        private Languages _selectedLanguage;


        private void Awake()
        {
            Instance = this;
        }

        public void LoadLang()
        {
            if (World.Instance)
                _selectedLanguage = World.Instance.Save.GetLanguage();
            else
            {
                var save = new Save();
                save.Init();
                _selectedLanguage = save.GetLanguage();
            }
            if (_selectedLanguage<0) _selectedLanguage = 0;
        }

        private void Start()
        {
            LoadLang();
            
           ProcessUIText();
           ProcessTMPText();
           onLoaded?.Invoke();
           
           enabled = false;
        }

        public Languages GetLanguage() => _selectedLanguage;
        
        
        private void ProcessUIText()
        {
            var text = FindObjectsOfTypeAll(typeof(UnityEngine.UI.Text));
            foreach (var item in (Text[]) text)
            {
#if UNITY_EDITOR
                if (PrefabUtility.GetPrefabParent(item)!=null)
                    continue;
#endif

                if (!item.text.Contains("{")) continue;

                var tmp = item.text;
                var begin = tmp.IndexOf('{');
                var end = tmp.IndexOf('}');
                var entry = tmp.Substring(begin+1, end-begin-1); 
                //Debug.Log(entry);
                /*item.text =  dataObject.data.GetEntry(entry, _selectedLanguage)*/;
                item.text = item.text.Replace("{"+entry+"}", dataObject.data.GetEntry(entry, _selectedLanguage));
            }
        }

        public string GetEntryValue(string name) =>
            dataObject.data.GetEntry(name.Substring(1, name.Length - 2), _selectedLanguage);
        
        private void ProcessTMPText()
        {
            var text = FindObjectsOfTypeAll(typeof(TextMeshProUGUI));
            foreach (var item in (TextMeshProUGUI[]) text)
            {
                
#if UNITY_EDITOR
                if (PrefabUtility.GetPrefabParent(item)!=null)
                    continue;
#endif
                
                if (!item.text.Contains("{")) continue;

                var tmp = item.text;
                var begin = tmp.IndexOf('{');
                var end = tmp.IndexOf('}');
                var entry = tmp.Substring(begin+1, end-begin-1);
//                Debug.Log(entry);
                /*item.text =  dataObject.data.GetEntry(entry, _selectedLanguage)*/;
                item.text = item.text.Replace("{"+entry+"}", dataObject.data.GetEntry(entry, _selectedLanguage));
            }
        }
    }
}