using UnityEngine;

namespace ClickerFramework.Multilanguage
{
    
    [CreateAssetMenu(fileName = "LanguageData", menuName = "Create LanguageData", order = 0)]
    public class LanguageData: ScriptableObject
    {
        [SerializeField] internal LanguageDictionary data = new LanguageDictionary();
    }
}