using System;
using UnityEngine;
using UnityEngine.UI;

namespace ClickerFramework
{
    public class AvatarController : MonoBehaviour
    {
        [SerializeField] private GameObject window;
        [SerializeField] private Image playerImage;
        [SerializeField] private GameObject FBAvatar;
        [SerializeField] private Image FBAvatarImage;
        private GameObject _root;

        public static AvatarController Instance;

        public void Start()
        {
            Instance = this;
            _root = window.transform.GetChild(1).gameObject;
            Debug.Log(World.Instance.Save.GetAvatarNum());
            if (World.Instance.Save.GetAvatarNum() <-1)
            {
                Debug.Log($"FB log status is {World.Instance.Save.FacebookData.Logged}");
                if (World.Instance.Save.FacebookData.Logged)
                {
                    var avatar = new Texture2D(160,160);
                    if (!avatar.LoadImage(World.Instance.Save.FacebookData.ProfilePicture))
                    {
                        Debug.Log("Field to load image");
                        window.SetActive(true);
                        return;
                    };
                    FBAvatarImage.sprite = Sprite.Create(avatar,new Rect(40,40,160,160), Vector2.zero);
                    FBAvatar.SetActive(true);
                }
                
                window.SetActive(true);
            }
            else
            {
                var avatar = new Texture2D(160,160);
                avatar.LoadImage(World.Instance.Save.FacebookData.ProfilePicture);
                FBAvatarImage.sprite = Sprite.Create(avatar,new Rect(40,40,160,160), Vector2.zero);
                playerImage.sprite = GetAvatar();
            }
        }

        public Sprite GetAvatar()
        {
            //Debug.Log($"Num: {_root.transform.childCount}");
            return World.Instance.Save.GetAvatarNum()>0 ? _root.transform.GetChild(World.Instance.Save.GetAvatarNum()).GetChild(0)
                .GetComponent<Image>().sprite : FBAvatarImage.sprite;
        }

        public void SelectAvatar(int ind)
        {
            AudioManager.Instance.PlaySecondSound("click");
            World.Instance.Save.SetAvatarNum(ind);
            playerImage.sprite = GetAvatar();
            window.SetActive(false);
        }
        
    }
}