﻿using System;
using System.Collections.Generic;
using BreakInfinity;
using ClickerFramework.Abstract;
using ClickerFramework.Extensions;
using ClickerFramework.Multilanguage;
using Facebook.Unity;
using Game;
using Spine.Unity;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace ClickerFramework
{
    /// <summary>
    /// Class that manage enemies
    /// </summary>
    public class LevelSystem : MonoBehaviour
    {
        /// <summary>
        /// Current set of enemies  
        /// </summary>
        private Level _level;

        /// <summary>
        /// Level index
        /// </summary>
        private int _index;

        /// <summary>
        /// Singleton
        /// </summary>
        public static LevelSystem Instance;
        

        /// <summary>
        /// All available enemies assets
        /// </summary>
        [SerializeField] private Enemy[] availableEnemies;
        
        /// <summary>
        /// Boss enemies assets 
        /// </summary>
        [SerializeField] private Enemy[] availableBossEnemies;

        /// <summary>
        /// Object that displays enemy
        /// </summary>
        [SerializeField] public EnemyObject enemyObject;

        /// <summary>
        /// Current enemy
        /// </summary>
        [SerializeField] public Enemy currentEnemy;

        /// <summary>
        /// Enemy index
        /// </summary>
        [HideInInspector] public int enemyIndex;

        /// <summary>
        /// Start enemy health
        /// </summary>
        private BigDouble _startHealth;

        /// <summary>
        /// Current enemy health
        /// </summary>
        private BigDouble _enemyCurrentHealth;

        /// <summary>
        /// Boss timer
        /// </summary>
        private float _timeToKill = 10;

        /// <summary>
        /// Enemy position on scene
        /// </summary>
        [FormerlySerializedAs("EnemyContent")] [SerializeField]
        private Transform enemyContent;

        /// <summary>
        /// Current base enemy health on level 
        /// </summary>
        [SerializeField] private BigDouble globalEnemyHealth;

        /// <summary>
        /// Current base boss health that depends on globalEnemyHealth
        /// </summary>
        private BigDouble globalBossHealth;

        public void Awake()
        {
            Instance = this;
        }

        private void Start()
        {
            //Calculates boss health and preparing level
            globalBossHealth = globalEnemyHealth * World.Instance.bossHealthMultiplier;
            SetNextLevel(true);
        }

        /// <summary>
        /// Selecting next enemy
        /// </summary>
        private void SelectNext()
        {
            _index++;
            if (_index >= _level.Enemies.Length)
            {
                SetNextLevel(true);
                return;
            }

            currentEnemy = _level.Enemies[_index];

            if (currentEnemy is UsualEnemy)
            {
                currentEnemy.Health = globalEnemyHealth;
                enemyObject.SetUsualIcon();
            }
            else{
                currentEnemy.Health = globalBossHealth;
                enemyObject.SetBossIcon();
            }


            enemyObject.graphics = Instantiate(currentEnemy.EnemyGameObject, enemyContent).GetComponent<Animator>();
            var enemyName = LanguageLoader.Instance.GetEntryValue(currentEnemy.Name.Split(' ')[0]);
            enemyObject.enemyName.text = currentEnemy.Name.Replace(currentEnemy.Name.Split(' ')[0], enemyName);
            var levelLabel = LanguageLoader.Instance.GetEntryValue("{levelLabel}");
            enemyObject.level.text = $"{levelLabel} {World.Instance.level}";
            enemyObject.levelCounter.text = $"{_index + 1}/10";
            currentEnemy.OnStart();
            _startHealth = currentEnemy.Health;
        }

        /// <summary>
        /// Generating new level
        /// </summary>
        /// <param name="levelUp"></param>
        public void SetNextLevel(bool levelUp = false)
        {
            _index = -1;
            if (levelUp)
                World.Instance.LevelChange(World.Instance.level + 1);
            _level = new Level(availableBossEnemies[Random.Range(0, availableBossEnemies.Length)], availableEnemies);
            if (enemyContent.childCount > 0)
                Destroy(enemyContent.GetChild(0).gameObject);
            globalEnemyHealth = World.Instance.CalculateEnemyHealth();
            globalBossHealth = globalEnemyHealth * World.Instance.bossHealthMultiplier;
            
            
            
            SelectNext();
        }

        private void Update()
        {
            currentEnemy.Update();
            CheckEnemyHealth();
        }


        /// <summary>
        /// Check enemy health and invoking all behaviuors
        /// </summary>
        private void CheckEnemyHealth()
        {
            if (currentEnemy.Health > 0)
                _enemyCurrentHealth = currentEnemy.Health;
            else
                _enemyCurrentHealth = 0;

            enemyObject.hpBarFill.fillAmount = (float) ((_enemyCurrentHealth / _startHealth)).ToDouble();
            enemyObject.hpBarText.text = NumberHelper.FormatNumber(_enemyCurrentHealth);
            
            if (currentEnemy.Health <= 1)
            {
                var reward = World.Instance.CalculateEarnedMoney();

                //calculate reward and in-game statistic.
                if (currentEnemy is BossEnemy)
                {
                    World.Instance.Save.GameStatistic.KilledBosses++;
                    //World.Instance.Save.SaveAll();
                    reward *= World.Instance.bossHealthMultiplier;
                }
                else
                {
                    World.Instance.Save.GameStatistic.KilledMonsters++;
                    //World.Instance.Save.SaveAll();
                }

                World.Instance.CurrentMoney += reward;
                
                //create ondie behaviour
                enemyObject.Die(() =>
                {
                    currentEnemy.OnDie();
                    SelectNext();
                });
            }
        }

        /// <summary>
        /// Damage enemy
        /// </summary>
        /// <param name="dmg">value of damage</param>
        public void DamageEnemy(BigDouble dmg)
        {
            currentEnemy.Health -= dmg;
            enemyObject.Damage();
        }
    }
}