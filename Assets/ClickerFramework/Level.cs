﻿using ClickerFramework.Abstract;
using UnityEngine;

namespace ClickerFramework
{
    /// <summary>
    /// Class for level (enemy order)
    /// </summary>
    [System.Serializable]
    public class Level
    {
        /// <summary>
        /// Available enemies
        /// </summary>
        [SerializeField] public Enemy[] Enemies = new Enemy[10];

        /// <summary>
        /// Creates enemy order
        /// </summary>
        /// <param name="boss">Boss object</param>
        /// <param name="enemies">Enemies</param>
        public Level(Enemy boss, params Enemy[] enemies)
        {
            for (int i = 0; i < Enemies.Length - 1; i++)
            {
                Enemies[i] = (Enemy) enemies[Random.Range(0, enemies.Length)].Clone();
            }

            Enemies[Enemies.Length - 1] = (Enemy) boss.Clone();
        }
    }
}