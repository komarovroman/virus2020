﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BreakInfinity;
using ClickerFramework;
using ClickerFramework.Abstract;
using ClickerFramework.Extensions;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Random = System.Random;

namespace ClickerFramework
{
    /// <summary>
    /// Class that controls player and tools behaviour 
    /// </summary>
    public class Player : MonoBehaviour
    {
        public static Player Instance;

        /// <summary>
        /// List of tools
        /// </summary>
        [SerializeField] public List<PassiveAttacker> attackers;

        /// <summary>
        /// Current player damage
        /// </summary>
        [SerializeField] private BigDouble playerDamage;

        /// <summary>
        /// Current player level
        /// </summary>
        public int level = 1;

        /// <summary>
        /// Minimum level upgrade price for progression
        /// </summary>
        [SerializeField] public BigDouble minUpgradePrice;

        /// <summary>
        /// Damage per second
        /// </summary>
        private BigDouble _activeDamagePerSecond, _attackersDamagePerSecond;

        private BigDouble _totalPassiveDamage = 0;
        public UnityEvent OnPlayerLevelUp;

#if !UNITY_EDITOR
        private GraphicRaycaster _raycaster;
#endif
        
        private void Awake()
        {
            Instance = this;

            //enable all bought tools
            foreach (var attacker in attackers.Where(x => !x.isInCompany))
            {
                if (World.Instance.Save.GetToolLevel(attacker.Name) >= 1)
                {
                    attacker.isActive = true;
                    /*Debug.Log($"{attacker.Name} {World.Instance.Save.GetToolLevel(attacker.Name)}");*/
                }
                else attacker.isActive = false;
            }
        }

        private void Start()
        {
            level = World.Instance.Save.GetPlayerLevel();
            playerDamage = NumberHelper.BigArithmeticProgression(1, World.Instance.playerDamageGrowth, level);
#if !UNITY_EDITOR   
            _raycaster = GameObject.Find("Canvas").GetComponent<GraphicRaycaster>();
#endif           
        }

        // Update is called once per frame
        void Update()
        {
            CheckPlayer();
            LevelSystem.Instance.currentEnemy.Health -= _totalPassiveDamage * (Time.deltaTime);
        }

        private void LateUpdate()
        {
            CalculateTotalAttackersDamage();
        }

        /// <summary>
        /// Calculates player damage
        /// </summary>
        /// <returns>damage per tap (click)</returns>
        public BigDouble GetPlayerDamage() => (((playerDamage) * World.Instance.GlobalDamageMultiplier)/**UnityEngine.Random.Range(1f,2f)*/).Round();

        //// <summary>
        //// Increase damage (deprecated)
        //// </summary>
        //public void IncPlayerDMG() => playerDamage += World.Instance.playerDamageGrowth;

        /// <summary>
        /// Calculate and cause total damage of tools
        /// </summary>
        /// <returns></returns>
        public void CalculateTotalAttackersDamage()
        {
            _totalPassiveDamage = 0;
            foreach (var attacker in attackers.Where(x => x.isActive && !x.isInCompany))
            {
                _totalPassiveDamage += attacker.Damage;
            }

            World.Instance.SetPassiveDamage(((_totalPassiveDamage * World.Instance.GlobalPassiveDamageMultiplier)).Round());
         
        }

        /// <summary>
        /// Check player input and damage enemies
        /// </summary>
        private void CheckPlayer()
        {
            
            if (World.Instance.MainMenu.SomethingOpen) return;
#if UNITY_EDITOR
            if (Input.GetMouseButton(0) || !Input.anyKeyDown) return;
#else
        if (!Input.GetMouseButtonDown(0)) return;
        
        var cursorData = new PointerEventData(null);
        cursorData.position = Input.mousePosition;
        var results = new List<RaycastResult>();
        _raycaster.Raycast(cursorData, results);
        if (results[0].gameObject.name != "ClickLayer")
            return;
#endif
            var dmg = GetPlayerDamage()*UnityEngine.Random.Range(1f,2f);
            AudioManager.Instance.PlaySound("enemy_kick");
            DamageEnemy(dmg, Color.white);
        }

        /// <summary>
        /// Damage enemy and spawn damage text
        /// </summary>
        /// <param name="dmg">damage value</param>
        /// <param name="color">text color</param>
        private void DamageEnemy(BigDouble dmg, Color color)
        {
            LevelSystem.Instance.DamageEnemy(dmg);
            World.Instance.textBalloon.Spawn(dmg, color);
        }

        /// <summary>
        /// Get tool from available list
        /// </summary>
        /// <param name="name">Tool name</param>
        /// <returns>tool</returns>
        public PassiveAttacker GetAttacker(string name) => attackers.FirstOrDefault(attacker => attacker.Name == name);

        public void CalculatePlayerDamage(int l = -1)
        {
            if (l < 0) l = level;
            playerDamage = NumberHelper.BigArithmeticProgression(1, World.Instance.playerDamageGrowth, l);
        }
        
        public BigDouble CalculatePrevPlayerDamage()
        {
            if (level - 1 > 0)
                return NumberHelper.BigArithmeticProgression(1, World.Instance.playerDamageGrowth, level - 1);
            else
                return 1;
        }
        
        public BigDouble CalculateNextPlayerDamage()
        { 
            return NumberHelper.BigArithmeticProgression(1, World.Instance.playerDamageGrowth, level + 1);
        }
        
    }
}