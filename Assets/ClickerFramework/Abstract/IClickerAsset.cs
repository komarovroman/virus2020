﻿using UnityEngine;

namespace ClickerFramework.Abstract
{
    /// <summary>
    /// Unity workaround for PreventSOSAve
    /// </summary>
    public interface IClickerAsset
    {
        
    }
}