﻿using System;
using System.Collections.Generic;
using BreakInfinity;
using ClickerFramework.Multilanguage;
using Facebook.Unity;
using Game;
using Spine.Unity;
using UnityEngine;
using UnityEngine.PlayerLoop;

namespace ClickerFramework.Abstract
{
    public interface IEnemy: ICloneable
    {
        GameObject EnemyGameObject { get; }
        float BaseHealth{get;}
        BigDouble Health { get; set; }
        
        void OnStart();
        void Update();
        void OnDie();
    }
    
    /// <summary>
    /// Base enemy class
    /// </summary>
    public abstract class Enemy: ScriptableObject, IEnemy, IClickerAsset
    {
        /// <summary>
        /// Base heals (deprecated)
        /// </summary>
        [SerializeField] protected float baseHealth;
        /// <summary>
        /// Linked gameobject
        /// </summary>
        [SerializeField] protected GameObject enemyGameObject;
        /// <summary>
        /// Enemy name
        /// </summary>
        [SerializeField] protected string enemyName;
        
        public virtual GameObject EnemyGameObject { get; }
        public virtual float BaseHealth { get; }
        public virtual BigDouble Health { get; set; }
        public virtual string Name { get; set; }
        
        /// <summary>
        /// On enemy spawn
        /// </summary>
        public virtual void OnStart()
        {
        }

        
        /// <summary>
        /// On enemy tick
        /// </summary>
        public virtual void Update()
        {
            
        }

        /// <summary>
        /// On enemy die
        /// </summary>
        public virtual void OnDie()
        {
            Telemetry.SendEvent(Telemetry.EnemyDie,  new Dictionary<string, object>()
            {
                [TelemetryEventParams.name] = LanguageLoader.Instance.GetEntryValue(Name)
            });
        }

        
        /// <summary>
        /// Cloning
        /// </summary>
        /// <returns>new instance of cloned object</returns>
        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}