﻿using System;
using ClickerFramework.Extensions;
using UnityEngine.Events;

namespace ClickerFramework.Abstract
{
    [Serializable]
    public enum ArtifactType
    {
        Common, Uncommon, Rare, Epic, Legendary, Mythic
    }
   
    public interface IArtifact
    {
        string Name { get;}
        string Description { get;}
        ArtifactType Type { get; }
        int Parts { get; set; }
        bool IsActive { get; }

        UnityEvent OnActivate { get; }
        ClickerIntEvent OnNewPartAdded { get; }

        void Init();
        void Activate();
    }
}