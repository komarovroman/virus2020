﻿using System.Collections;
using System.Collections.Generic;
using ClickerFramework.Multilanguage;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TutorialController : MonoBehaviour
{

    [SerializeField] private GameObject[] pages;
    [SerializeField] private GameObject[] dots;
    [SerializeField] private string[] pagesText;
    [SerializeField] private Text pageText;
    private int _index = 0;

    // Start is called before the first frame update
    void Start()
    {
        foreach (var page in pages)
            page.SetActive(false);
        SetPageActive(0);
    }

    
    public void SetPageActive(int index)
    {
        pages[_index].SetActive(false);
        dots[_index].transform.GetChild(0).gameObject.SetActive(false);
        
        _index = index;
        pages[_index].SetActive(true);
        dots[_index].transform.GetChild(0).gameObject.SetActive(true);
        
        pageText.text = LanguageLoader.Instance.GetEntryValue("{"+pagesText[_index]+"}");
    }

    public void SetPrevPageActive()
    {
        if (_index-1>=0)
            SetPageActive(_index-1);
    }

    public void SetNextPageActive()
    {
        if (_index + 1 < pages.Length)
            SetPageActive(_index+1);
        else LoadGame();
    }

    public void LoadGame() => SceneManager.LoadScene(3);
    
    // Update is called once per frame
    void Update()
    {
        
    }
}
