﻿using System;
using System.Collections;
using Spine.Unity;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

/// <summary>
/// Class that manage enemy on game scene
/// </summary>
public class EnemyObject : MonoBehaviour
{
    /// <summary>
    /// Enemy graphics
    /// </summary>
    public Animator graphics;

    /// <summary>
    /// Enemy name
    /// </summary>
    public Text enemyName;

    /// <summary>
    /// level
    /// </summary>
    public Text level;

    /// <summary>
    /// plevel
    /// </summary>
    public Text levelCounter;

    /// <summary>
    /// boss timer (if needed)
    /// </summary>
    public Image timer;
    
    public Image icon;

    /// <summary>
    /// Enemy HP bar
    /// </summary>
    public Image hpBarFill;

    /// <summary>
    /// Enemy HP Bar text
    /// </summary>
    public Text hpBarText;

    [SerializeField] private Sprite usualVirus, bossVirus; 
    
    private void Start()
    {
    }

    private void Update()
    {
        /*var curr = graphics.GetFloat("Blend");
        var @new = Mathf.Lerp(curr, 0,0.025f);
        graphics.SetFloat("Blend", @new);*/
    }

    public void SetUsualIcon() => icon.sprite = usualVirus;
    public void SetBossIcon() => icon.sprite = bossVirus;
    
    /// <summary>
    /// On die coroutine (deprecated)
    /// </summary>
    /// <param name="onDie"></param>
    public void Die(UnityAction onDie) => StartCoroutine(EnemyDie(onDie));

    private IEnumerator EnemyDie(UnityAction onDie)
    {
        if (graphics != null) 
            Destroy(graphics.gameObject);
        yield return new WaitForEndOfFrame();

        onDie.Invoke();
    }

    /// <summary>
    /// For controlling enemy animation
    /// </summary>
    public void Damage()
    {
        graphics.SetTrigger("Damage");
    }
}