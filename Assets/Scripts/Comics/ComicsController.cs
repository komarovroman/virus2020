﻿using System;
using System.Collections;
using System.Collections.Generic;
using ClickerFramework;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ComicsController : MonoBehaviour
{
    // Start is called before the first frame update
    private bool _locked = false;
    private int _index = 0;
    [SerializeField] private Sprite[] rusImages;
    [SerializeField] private Sprite[] enImages;
    [SerializeField] private Image[] pages;

    void Start()
    {
        _index = pages.Length;

        var s = new Save();
        s.Init();

        int ind = (int) s.GetLanguage();

        for (int i = 0; i < pages.Length; i++)
        {
            if (ind == 0)
                pages[(pages.Length-1)-i].sprite = rusImages[i];
            else    
                pages[(pages.Length-1)-i].sprite = enImages[i];
        }
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (!_locked && Input.GetMouseButtonDown(0))
        {
            _index--;
            if (_index > 0)
                StartCoroutine(Fade(pages[_index]));
            else
                StartCoroutine(Fade(pages[_index], () => SceneManager.LoadScene(2)));
        }
    }

    private IEnumerator Fade(Image img, Action after = null)
    {
        var color = img.color;
        _locked = true;
        while (color.a > 0)
        {
            color.a -= Time.fixedDeltaTime;
            img.color = color;
            yield return null;
        }

        _locked = false;
        after?.Invoke();
    }
}