using System;
using System.Collections;
using System.Linq;
using ClickerFramework;
using ClickerFramework.Multilanguage;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Comics
{
    public class InGameComicsController : MonoBehaviour
    {
        [SerializeField] private GameObject pagesDomain;
        [SerializeField] private Image[] pages;

        [SerializeField] private Sprite[] countryPages;
        [SerializeField] private Sprite[] continentsPages;
        
        private string _suffix;
        private int _index = -1;
        private int _to = 0;
        private bool _locked = false;
        
        private void Start()
        {
            _suffix = LanguageLoader.Instance.GetLanguage() == Languages.English ? "_En" : "";
            World.Instance.OnLevelChange += OnLevelCompleted;
        }

        private void OnLevelCompleted(int levelNum)
        {
            if (!Save.Countries.ContainsKey(levelNum)) return;
            var name = Save.Countries[levelNum].Replace(" ", "_");
            if (World.Instance.Save.IsCountryCompleted(name)) return;
            pagesDomain.SetActive(true);
            
            World.Instance.Save.SetCountryAsCompleted(name);
            var fullName = $"Country{Random.Range(1, 2)}_1_{name + _suffix}";
            if (countryPages.FirstOrDefault(x => x.name == fullName))
            {
                pages[3].sprite = countryPages.FirstOrDefault(x => x.name == fullName);
                Debug.Log($"3: {fullName}");

                if (_suffix == "")
                {
                    fullName = $"Country{Random.Range(1, 3)}_{Random.Range(2, 4)}";
                    Debug.Log($"2: {fullName}");
                    pages[2].sprite = countryPages.FirstOrDefault(x => x.name == fullName);
                }
                else
                {
                    fullName = $"Country{Random.Range(1, 3)}_2{_suffix}";
                    Debug.Log($"2: {fullName}");
                    pages[2].sprite = countryPages.FirstOrDefault(x => x.name == fullName);
                }

                pages[3].color = pages[2].color = new Color(1, 1, 1, 1);
                _index = 3;
                _to = 2;
            }else _index = 2;

            if (!Save.Continents.ContainsKey(levelNum)) return;
            name = Save.Continents[levelNum].Replace(" ", "_");
            if (World.Instance.Save.IsContinentCompleted(name)) return;
            World.Instance.Save.SetContinentAsCompleted(name);
            fullName = $"Continent_1_{name + _suffix}";
            Debug.Log($"1: {fullName}");
            pages[1].sprite = continentsPages.FirstOrDefault(x => x.name == fullName);
            
            fullName = $"Continent_2{_suffix}";
            Debug.Log($"0: {fullName}");
            pages[0].sprite = continentsPages.FirstOrDefault(x => x.name == fullName);
            

            pages[0].color = pages[1].color = new Color(1, 1, 1, 1);
            _to = 0;
        }
        
        void LateUpdate()
        {
            if (_index < 0) return;
            if (!_locked && Input.GetMouseButtonDown(0))
            {
                StartCoroutine(_index > _to ? Fade(pages[_index]) : Fade(pages[_index], CleanUp));
                _index--;
            }
        }

        private void CleanUp()
        {
            pagesDomain.SetActive(false);
            foreach (var page in pages)
            {
                page.color = new Color(0, 0, 0, 0);
            }

            _index = -1;
        }
        
        private IEnumerator Fade(Image img, Action after = null)
        {
            var color = img.color;
            _locked = true;
            while (color.a > 0)
            {
                color.a -= Time.fixedDeltaTime*2f;
                img.color = color;
                yield return null;
            }

            _locked = false;
            after?.Invoke();
        }
        
        
    }
}