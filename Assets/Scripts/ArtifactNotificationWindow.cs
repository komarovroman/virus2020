﻿using System.Collections;
using System.Collections.Generic;
using ClickerFramework.Multilanguage;
using UnityEngine;
using UnityEngine.UI;

public class ArtifactNotificationWindow : MonoBehaviour
{
    
    [SerializeField] private Image artifactSprite;
    [SerializeField] private Text windowText;
    [SerializeField] private GameObject window;
    public static ArtifactNotificationWindow Instance;

    void Awake()
    {
        Instance = this;
    }

    public void Show(string artifactName, Sprite icon, bool isLast = false)
    {
        if (!isLast)
            windowText.text = LanguageLoader.Instance.GetEntryValue("{artifactonotetext}") + " <color=purple>" +
                              LanguageLoader.Instance.GetEntryValue(artifactName)+"</color>";
        else 
            windowText.text = LanguageLoader.Instance.GetEntryValue("{fullartifactonotetext}") + " <color=purple>" +
                               LanguageLoader.Instance.GetEntryValue(artifactName)+"</color>";
        
        artifactSprite.sprite = icon;
        window.SetActive(true);
    }
    
    public void Close()
    {
        window.SetActive(false);
    }
}
