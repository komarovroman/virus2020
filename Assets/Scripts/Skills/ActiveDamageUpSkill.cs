﻿using ClickerFramework;
using ClickerFramework.SkillTree;
using UnityEngine;
using UnityEngine.Serialization;

namespace Skills
{
    [CreateAssetMenu(fileName = "ActiveDamageUpSkill", menuName = "Create ActiveDamageUpSkill", order = 0)]
    public class ActiveDamageUpSkill : Skill
    {
        [SerializeField] [Range(0, 1)] public float multiplier;

        public override void OnDiscover()
        {
            World.Instance.GlobalDamageMultiplier += multiplier;
            base.OnDiscover();
        }
    }
}