﻿using ClickerFramework;
using ClickerFramework.SkillTree;
using UnityEngine;
using UnityEngine.Serialization;

namespace Skills
{
    [CreateAssetMenu(fileName = "PassiveDamageUpSkill", menuName = "Create PassiveDamageUpSkill", order = 0)]
    public class PassiveDamageUpSkill : Skill
    {
        [SerializeField] [Range(0, 1)] public float multiplier;

        public override void OnDiscover()
        {
            World.Instance.GlobalPassiveDamageMultiplier += multiplier;
            base.OnDiscover();
        }
    }
}