﻿using ClickerFramework;
using ClickerFramework.SkillTree;
using UnityEngine;
using UnityEngine.Serialization;

namespace Skills
{
    [CreateAssetMenu(fileName = "MoneyUpSkill", menuName = "Create MoneyUpSkill", order = 0)]
    public class MoneyUpSkill : Skill
    {
        [SerializeField] [Range(0, 1)] public float multiplier;

        public override void OnDiscover()
        {
            World.Instance.GlobalMoneyMultiplier += multiplier;
            base.OnDiscover();
        }
    }
}