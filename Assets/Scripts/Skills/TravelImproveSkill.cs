﻿using ClickerFramework;
using ClickerFramework.SkillTree;
using UnityEngine;
using UnityEngine.Serialization;

namespace Skills
{
    [CreateAssetMenu(fileName = "InGameMoneyUpSkill", menuName = "Create InGameMoneyUpSkill", order = 0)]
    public class TravelImproveSkill : Skill
    {
        [SerializeField] [Range(0, 1)] public float timeDisMultiplier;
        [SerializeField] [Range(0, 1)] public float rewardMultiplier;

        public override void OnDiscover()
        {
            base.OnDiscover();
            World.Instance.GlobalTravelTimeMultiplier -= timeDisMultiplier;
            World.Instance.GlobalTravelRewardsMultiplier += rewardMultiplier;
        }
    }
}