﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using ClickerFramework;
using ClickerFramework.Extensions;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// In game world map controller
/// </summary>
public class MapController : MonoBehaviour
{
    /// <summary>
    /// Base line
    /// </summary>
    [SerializeField] private GameObject line;

    /// <summary>
    /// Root RectTransform
    /// </summary>
    private RectTransform Base;

    /// <summary>
    /// Lines root transform
    /// </summary>
    [SerializeField] private Transform LineDomain;

    /// <summary>
    /// All map dots
    /// </summary>
    private Image[] _mapDots;

    /// <summary>
    /// All lines
    /// </summary>
    private LineRenderer[] _mapLines;

    
    [SerializeField] private Sprite completedDot;
    [SerializeField] private Sprite uncompletedDot;
    [SerializeField] private Sprite completedLine;
    [SerializeField] private Sprite uncompletedLine;
    [SerializeField] private GameObject levelPoint;
    [SerializeField] private Text levelPointText;

    private Vector3 basePosition;

    static public bool IsVisible = false;
    // Start is called before the first frame update
    void Start()
    {
        var scaler = GameObject.Find("Canvas").GetComponent<CanvasScaler>();
        
        _mapDots = Array.ConvertAll(GameObject.FindGameObjectsWithTag("MapDot"),
            (x) => x.GetComponent<Image>());
        Base = GetComponent<RectTransform>();
        _mapLines = new LineRenderer[_mapDots.Length];

        int parse(string num)
        {
            return int.TryParse(num, out var n) ? n : 0;
        }

        _mapDots = _mapDots.OrderBy(x => parse(Regex.Match(x.gameObject.name, @"\d+").Value)).ToArray();

        //Selecting dots in order and connect by lines
        for (int i = 1; i < _mapDots.Length; i++)
        {
            var from = Base.TransformPoint(_mapDots[i - 1].transform.localPosition);
            var to = Base.TransformPoint(_mapDots[i].transform.localPosition);
            //var from = Camera.main.ScreenToWorldPoint(_mapDots[i - 1].transform.position);
            //var to = Camera.main.ScreenToWorldPoint(_mapDots[i].transform.position);
            from.z = 0;
            to.z = 0;

            var l = Instantiate(line, LineDomain).GetComponent<LineRenderer>();
            l.gameObject.transform.position = Vector3.zero;
            
            var pos = l.transform.localPosition;
            pos.z = 0;
            l.transform.localPosition = pos;
                        
            l.material.mainTexture = uncompletedLine.texture;

            l.SetPosition(0, from);
            l.SetPosition(1, to);
            
            
            _mapLines[i - 1] = l;
        }

        //Update map due to current level
        UpdateMap(World.Instance.Save.GetReachedLevel());
        World.Instance.OnLevelChange += UpdateMap;

        scaler.matchWidthOrHeight = 0;
        scaler.uiScaleMode = UnityEngine.UI.CanvasScaler.ScaleMode.ScaleWithScreenSize;
        basePosition = Base.transform.position;
    }


    public void SetPointerState(bool enabled)
    {
        levelPoint.SetActive(IsVisible = enabled);
        if (enabled)
            UpdateMap(World.Instance.level);
    }
    
    /// <summary>
    /// Update map to max reached level or custom
    /// </summary>
    /// <param name="currentLevel">level</param>
    void UpdateMap(int currentLevel)
    {
        var level = NumberHelper.LoopNumber(World.Instance.Save.GetReachedLevel()-1, _mapDots.Length);
        int r = ((World.Instance.Save.GetReachedLevel() - 1) / (_mapDots.Length-1))-1;
        //var level = _mapDots.Length - 1;
        currentLevel--;
        
        for (int i = 0, j = 0; i < _mapDots.Length; j = i++)
        {
            if (i > level)
            {
                _mapDots[i-1].sprite = uncompletedDot;
                _mapLines[j].material.mainTexture = uncompletedLine.texture;
                continue;
            }

            var l = (r * _mapDots.Length) + i + 1;
            _mapDots[i].GetComponent<MapSelectable>().level = l;
            //Debug.Log($"{i}: {l} = ({r}*{_mapDots.Length})+{i}+1");
            _mapDots[i].sprite = completedDot;
            _mapLines[j].material.mainTexture = completedLine.texture;

            if (i == NumberHelper.LoopNumber(currentLevel, _mapDots.Length))
            {
                var pos = _mapDots[i].transform.localPosition;
                pos.z = -198;
                pos.y += levelPoint.GetComponent<RectTransform>().rect.height / 2f;
                levelPoint.transform.localPosition = pos;
                levelPointText.text = (currentLevel + 1).ToString();
            }
        }
    }
}