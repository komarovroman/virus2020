﻿using System.Collections;
using System.Collections.Generic;
using AppsFlyerSDK;
using ClickerFramework;
using Facebook.Unity;
using Game;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartupController : MonoBehaviour
{
    
    private Save _save; 
    [SerializeField] private GameObject langSelectWindow;
    
    void Awake ()
    {
        Telemetry.Init(this);
        _save = GetComponent<Save>();
        
        
    }
    
    
    void Start()
    {
        FB.Init(InitCallback, OnHideUnity);
        
        Amplitude amplitude = Amplitude.Instance;
        amplitude.logging = true;
        amplitude.trackSessionEvents(false);
        amplitude.init("ccc9c04d177591bc46845c90e7005160");
        
        if (!_save.IsFirstTime())
        {
           
            SceneManager.LoadScene(3);
            return;
        }

/*#if  !UNITY_EDITOR
        var perms = new List<string>() {"public_profile", "email"};
        FB.LogInWithReadPermissions(perms, AuthCallback);
#endif*/
        //FB.ActivateApp();
        
        langSelectWindow.SetActive(true);
    }
    
    void Update()
    {
        
    }

    private void AuthCallback (ILoginResult result) {
        if (FB.IsLoggedIn) {
            var aToken = AccessToken.CurrentAccessToken;
            Debug.Log($"User {aToken.UserId} sing ins");
            
            FB.API("/me?fields=name", HttpMethod.GET, (aresult) =>
            {
                foreach (var keyValuePair in aresult.ResultDictionary)
                {
                    var res = keyValuePair.Value;
                    if (res is Dictionary<string, object> erlog)
                    {
                        Debug.Log("Error with me?fields=first_name, last_name");
                        foreach (var keyValuePair2 in erlog)
                        {
                            Debug.Log($"Founded {keyValuePair2.Key} with {keyValuePair2.Value}");
                        }
                    }else
                        Debug.Log($"Founded {keyValuePair.Key} with {keyValuePair.Value}");
                }
                if (!aresult.ResultDictionary.ContainsKey("name")) return;
                var name = (string) aresult.ResultDictionary["name"];
                _save.FacebookData.UserName = name;
                Debug.Log($"Gotten user name {name}");
            });
            var www = new WWW("http://graph.facebook.com/" + aToken.UserId + "/picture?width=240&height=240&type=square&redirect=true");
            Debug.Log ("http://graph.facebook.com/" + aToken.UserId + "/picture?width=210&height=210&type=normal&redirect=true"+"\t"+www.error);

            while (!www.isDone) {
                Debug.Log ("waiting " + www.bytesDownloaded);
            }

            _save.FacebookData.ProfilePicture = www.texture.EncodeToJPG(50);
            Debug.Log($"Gotten user picture { _save.FacebookData.ProfilePicture.Length}");
            /*FB.API("/me/picture?type=square&height=160&width=160", HttpMethod.GET, (aresult) =>
            {
                foreach (var keyValuePair in aresult.ResultDictionary)
                {
                    var res = keyValuePair.Value;
                    if (res is Dictionary<string, object> erlog)
                    {
                        Debug.Log("Error with /me/picture?type=square&height=160&width=160");
                        foreach (var keyValuePair2 in erlog)
                        {
                            Debug.Log($"Founded {keyValuePair2.Key} with {keyValuePair2.Value}");
                        }
                    }else
                        Debug.Log($"Founded {keyValuePair.Key} with {keyValuePair.Value}");
                }
                if (aresult.Texture == null) return;
                _save.FacebookData.ProfilePicture = aresult.Texture.EncodeToJPG(50);
                Debug.Log($"Gotten user picture { _save.FacebookData.ProfilePicture.Length}");
            });*/
            
            _save.FacebookData.Logged = true;
            _save.SaveAllNow();
        } else {
            Debug.Log("User cancelled login");
        }
    }
    
    private void InitCallback ()
    {
        if (FB.IsInitialized) {
            // Signal an app activation App Event
            FB.ActivateApp();
            if (!_save.FacebookData.Logged) FB.LogInWithReadPermissions (
                new List<string>(){"public_profile", "email"},
                AuthCallback
            );

            
            if (!_save.IsFirstTime())
                Telemetry.SendEvent(Telemetry.AppOpen, null);
            _save.SaveAllNow();
        } else {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }

       
    }

    public void SetLang(int index)
    {
        _save.SetLanguage(index);
        _save.GameStartedFirstTime();
        _save.SaveAllNow();
        SceneManager.LoadScene(1);
    }
    
    private void OnHideUnity (bool isGameShown)
    {
        if (!isGameShown) {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        } else {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }
}
