using ClickerFramework;
using ClickerFramework.Tasks;
using UnityEngine;

namespace Tasks
{
    [CreateAssetMenu(fileName = "Task", menuName = "Tasks/Create BuyHero Task", order = 0)]
    public class TaskBuyAnyHero: Task
    {
        protected override void SetupListeners()
        {
            World.Instance.Save.GameStatistic.OnAnyHeroBought.AddListener(Check);
        }

        protected override void Check()
        {
            Completed = true;
            onCompleted.Invoke();
        }
    }
}