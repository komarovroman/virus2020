using ClickerFramework;
using ClickerFramework.Tasks;
using UnityEngine;

namespace Tasks
{
    [CreateAssetMenu(fileName = "Task", menuName = "Tasks/Create Donate Task", order = 0)]
    public class TaskDonate: Task
    {
        protected override void SetupListeners()
        {
            World.Instance.Save.GameStatistic.OnDonated.AddListener(Check);
        }

        protected override void Check()
        {
            Completed = true;
            onCompleted.Invoke();
        }
    }
}