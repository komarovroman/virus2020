using ClickerFramework;
using ClickerFramework.Tasks;
using UnityEngine;

namespace Tasks
{
    [CreateAssetMenu(fileName = "Task", menuName = "Tasks/Create CollectArtifacts Task", order = 0)]
    public class TaskCollectArtifacts : Task
    {
        [SerializeField] private int count;

        protected override void SetupListeners()
        {
            World.Instance.Save.GameStatistic.OnArtifactCollected.AddListener(Check);
        }

        protected override void Check()
        {
            if (World.Instance.Save.GameStatistic.ArtifactCount < count) return;
            Completed = true;
            onCompleted.Invoke();
        }
    }
}