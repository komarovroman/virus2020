using System.Linq.Expressions;
using ClickerFramework;
using ClickerFramework.Tasks;
using UnityEngine;

namespace Tasks
{
    [CreateAssetMenu(fileName = "Task", menuName = "Tasks/Create ClearCountry Task", order = 0)]
    public class TaskClearCountry : Task
    {
        [SerializeField] private int mode;

        protected override void SetupListeners()
        {
            switch (mode)
            {
                case 1:
                {
                    World.Instance.Save.GameStatistic.OnClearedFirstPartOfCountries.AddListener(Check);
                    break;
                }
                case 2:
                {
                    World.Instance.Save.GameStatistic.OnClearedSecondPartOfCountries.AddListener(Check);
                    break;
                }
                case 3:
                {
                    World.Instance.Save.GameStatistic.OnClearedThirdPartOfCountries.AddListener(Check);
                    break;
                }
            }
        }

        protected override void Check()
        {
            Completed = true;
            onCompleted.Invoke();
        }
    }
}