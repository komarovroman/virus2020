using BreakInfinity;
using ClickerFramework;
using ClickerFramework.Tasks;
using UnityEngine;

namespace Tasks
{
    [CreateAssetMenu(fileName = "Task", menuName = "Tasks/Create EarnMoney Task", order = 0)]
    public class TaskEarnMoney : Task
    {
        [SerializeField] private BigDouble money;

        protected override void SetupListeners()
        {
            World.Instance.Save.GameStatistic.OnTotalMoneyChanged.AddListener(Check);
        }

        protected override void Check()
        {
            if (World.Instance.Save.GameStatistic.TotalMoney < money) return;

            Completed = true;
            onCompleted.Invoke();
        }
    }
}