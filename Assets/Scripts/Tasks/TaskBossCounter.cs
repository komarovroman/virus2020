using ClickerFramework;
using ClickerFramework.Tasks;
using UnityEngine;

namespace Tasks
{
    [CreateAssetMenu(fileName = "Task", menuName = "Tasks/Create BossCounter Task", order = 0)]
    public class TaskBossCounter : Task
    {
        [SerializeField] private int bossCount;

        protected override void SetupListeners()
        {
            World.Instance.Save.GameStatistic.OnKilledBossesChanged.AddListener(Check);
        }

        protected override void Check()
        {
            //Debug.Log($"{monsterCount} > {World.Instance.Save.GetGameStatistic.KilledMonsters}");
            if (bossCount > World.Instance.Save.GameStatistic.KilledBosses) return;

            Completed = true;

            //World.Instance.Save.GetGameStatistic.OnKilledMonstersChanged.RemoveListener(Check);

            onCompleted?.Invoke();
        }
    }
}