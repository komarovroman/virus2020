using ClickerFramework;
using ClickerFramework.Tasks;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace Tasks
{
    [CreateAssetMenu(fileName = "Task", menuName = "Tasks/Create EnemyCounter Task", order = 0)]
    public class TaskEnemyCounter : Task

    {
        [SerializeField] private int monsterCount;

        protected override void SetupListeners()
        {
            World.Instance.Save.GameStatistic.OnKilledMonstersChanged.AddListener(Check);
        }

        protected override void Check()
        {
            //Debug.Log($"{monsterCount} > {World.Instance.Save.GetGameStatistic.KilledMonsters}");
            if (monsterCount > World.Instance.Save.GameStatistic.KilledMonsters) return;

            Completed = true;

            //World.Instance.Save.GetGameStatistic.OnKilledMonstersChanged.RemoveListener(Check);

            onCompleted?.Invoke();
        }
    }
}