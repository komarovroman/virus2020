﻿using System.Collections;
using System.Collections.Generic;
using ClickerFramework;
using UnityEngine;
using UnityEngine.EventSystems;

public class MapSelectable : MonoBehaviour, IPointerDownHandler
{
    /// <summary>
    /// Saved level index
    /// </summary>
    public int level;

    // Start is called before the first frame update
    void Start()
    {
        level = -1;
    }

    // Update is called once per frame
    void Update()
    {
    }

    /// <summary>
    /// Level dot button
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log($"{!MapController.IsVisible} {level<0} {World.Instance.level == level} {World.Instance.Save.GetReachedLevel()<level-1}");
        if (!MapController.IsVisible || level < 0 || World.Instance.level == level ||
            World.Instance.Save.GetReachedLevel() < level) return;
        
        World.Instance.LevelChange(level);
        LevelSystem.Instance.SetNextLevel();
    }
}