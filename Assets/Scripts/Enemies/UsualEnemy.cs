﻿using BreakInfinity;
using ClickerFramework;
using ClickerFramework.Abstract;
using Spine.Unity;
using UnityEngine;

/// <summary>
/// Simple enemy
/// </summary>
[CreateAssetMenu(fileName = "Enemy", menuName = "Create UsualEnemy", order = 0)]
public class UsualEnemy : Enemy
{
    public override GameObject EnemyGameObject => enemyGameObject;
    public override float BaseHealth => baseHealth;
    public override string Name => enemyName;

    public override BigDouble Health { get; set; }

    public override void OnStart()
    {
    }

    public override void OnDie()
    {
        AudioManager.Instance.PlaySound("enemy_die");
        AudioManager.Instance.PlaySecondSound("earn_money");
        
        base.OnDie();
    }
}