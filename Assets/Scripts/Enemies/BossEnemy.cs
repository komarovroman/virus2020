﻿using BreakInfinity;
using ClickerFramework;
using ClickerFramework.Abstract;
using ClickerFramework.Artifacts;
using Spine.Unity;
using UnityEngine;

/// <summary>
/// Boss enemy with timer logic
/// </summary>
[CreateAssetMenu(fileName = "BossEnemy", menuName = "Create BossEnemy", order = 0)]
public class BossEnemy : Enemy
{
    public override GameObject EnemyGameObject => enemyGameObject;
    public override float BaseHealth => baseHealth;
    public override string Name => enemyName;

    public override BigDouble Health { get; set; }
    [SerializeField] private float killTimer;

    private float baseTimer;
    public float KillTimer => killTimer;

    public override void OnStart()
    {
        //Health = baseHealth*World.Instance.healthMultiplier;
        baseTimer = killTimer;
    }

    public override void Update()
    {
        killTimer -= Time.fixedDeltaTime;
        if (killTimer <= 0)
        {
            LevelSystem.Instance.enemyObject.levelCounter.text = "1/10";
            LevelSystem.Instance.SetNextLevel(false);
        }else LevelSystem.Instance.enemyObject.levelCounter.text = (KillTimer).ToString("F1");
    }

    public override void OnDie()
    {
        LevelSystem.Instance.enemyObject.timer.fillAmount = 0;
        ArtifactManager.Instance.GenerateNextArtifact();
        AudioManager.Instance.PlaySound("boss_die");
        AudioManager.Instance.PlaySecondSound("earn_money");
        
        base.OnDie();
    }
}