﻿using System;
using System.Collections;
using System.Collections.Generic;
using ClickerFramework;
using UnityEngine;
using UnityEngine.Purchasing;

public class IAPManager : MonoBehaviour, IStoreListener
{

    private static IStoreController _storeController;
    private static IExtensionProvider _storeExtensionProvider;

    private string _productbuy5 = "buy5crystals";
    private string _productbuy80 = "buy80crystals";
    private string _productbuy150 = "buy150crystals";
    private string _productbuy750 = "buy750crystals";
    private string _productbuy2500 = "buy2500crystals";

    // Start is called before the first frame update
    void Start()
    {
        InitializePurchasing();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void InitializePurchasing()
    {
        if (IsInitialized()) return;
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        builder.AddProduct(_productbuy5, ProductType.Consumable);
        builder.AddProduct(_productbuy80, ProductType.Consumable);
        builder.AddProduct(_productbuy150, ProductType.Consumable);
        builder.AddProduct(_productbuy750, ProductType.Consumable);
        builder.AddProduct(_productbuy2500, ProductType.Consumable);
        
        UnityPurchasing.Initialize(this, builder);
    }

    public void BuyItem(int index)
    {
        switch (index)
        {
            case 0:
                BuyProductID(_productbuy5);
                break;
            case 1:
                BuyProductID(_productbuy80);
                break;
            case 2:
                BuyProductID(_productbuy150);
                break;
            case 3:
                BuyProductID(_productbuy750);
                break;
            case 4:
                BuyProductID(_productbuy2500);
                break;
        }
    }
    
    private void BuyProductID(string productId)
    {
        // If Purchasing has been initialized ...
        if (IsInitialized())
        {
            // ... look up the Product reference with the general product identifier and the Purchasing 
            // system's products collection.
            Product product = _storeController.products.WithID(productId);

            // If the look up found a product for this device's store and that product is ready to be sold ... 
            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                // asynchronously.
                _storeController.InitiatePurchase(product);
            }
            // Otherwise ...
            else
            {
                // ... report the product look-up failure situation  
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        // Otherwise ...
        else
        {
            // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
            // retrying initiailization.
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }
    
    public bool IsInitialized() => _storeController != null && _storeExtensionProvider != null;
    
    
    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.Log("Store initialize failed:" + error);
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        // A consumable product has been purchased by this user.
        if (String.Equals(args.purchasedProduct.definition.id, _productbuy5, StringComparison.Ordinal))
        {
            World.Instance.AddCrystals(5);
        }
        else if (String.Equals(args.purchasedProduct.definition.id, _productbuy80, StringComparison.Ordinal))
        {
            World.Instance.AddCrystals(80);
        }
        else if (String.Equals(args.purchasedProduct.definition.id, _productbuy150, StringComparison.Ordinal))
        {
            World.Instance.AddCrystals(150);
        }
        else if (String.Equals(args.purchasedProduct.definition.id, _productbuy750, StringComparison.Ordinal))
        {
            World.Instance.AddCrystals(750);
        }
        else if (String.Equals(args.purchasedProduct.definition.id, _productbuy2500, StringComparison.Ordinal))
        {
            World.Instance.AddCrystals(2500);
        }
        else
        {
            Debug.Log($"Unrecognized product: '{args.purchasedProduct.definition.id}'");
        }
        
        World.Instance.Save.GameStatistic.Donated = true;
        AudioManager.Instance.PlaySound("earn_crystals");
        return PurchaseProcessingResult.Complete;
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        Debug.Log($"OnPurchaseFailed: FAIL. Product: '{product.definition.storeSpecificId}', PurchaseFailureReason: {failureReason}");
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        _storeController = controller;
        _storeExtensionProvider = extensions;
    }
}
