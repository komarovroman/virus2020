﻿using System;
using System.Collections;
using System.Collections.Generic;
using ClickerFramework;
using ClickerFramework.Extensions;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    /// <summary>
    /// UI animator
    /// </summary>
    private Animator _animator;

    /// <summary>
    /// UI animator state var
    /// </summary>
    private static readonly int State = Animator.StringToHash("State");

    /// <summary>
    /// Background
    /// </summary>
    [SerializeField] private Image BackgroundImage;

    [SerializeField] private Image OverlayImage;
    
    /// <summary>
    /// Background images
    /// </summary>
    [SerializeField] private Sprite[] Backgrounds;

    public bool SomethingOpen = false;

    [SerializeField] private GameObject notEnoughCrystalsWindow;
    public static Menu Instance;
    
    private void Awake()
    {
        Instance = this;
        OverlayImage.gameObject.SetActive(true);
    }

    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        World.Instance.OnLevelChange += SetBackgroundImage;
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void StartGame()
    {
        StartCoroutine(HideOverlay());
    }

    public void SetNotEnoughCrystalsWindowState(bool state) => notEnoughCrystalsWindow.SetActive(state);
    
    private IEnumerator HideOverlay()
    {
        yield return new WaitForEndOfFrame();
        
        var text = OverlayImage.transform.GetChild(0).GetComponent<Text>();
        var imgColor = OverlayImage.color;
        var textColor = text.color;
        
        while (imgColor.a > 0)
        {
            imgColor.a -= Time.fixedDeltaTime * 10;
            textColor.a -= Time.fixedDeltaTime * 10;
            OverlayImage.color = imgColor;
            text.color = textColor;
            yield return null;
        }
        OverlayImage.gameObject.SetActive(false);
    }
    
    /// <summary>
    /// Loop backgrounds array and set by level
    /// </summary>
    /// <param name="currentLevel">level</param>
    private void SetBackgroundImage(int currentLevel)
    {
        Debug.Log($"Setting background image {Backgrounds.Length} {currentLevel}");
        var ind = (int)NumberHelper.LoopNumber(currentLevel, Backgrounds.Length);
        
        BackgroundImage.sprite = Backgrounds[ind];
    }

    /// <summary>
    /// Controlling the animator
    /// </summary>
    /// <param name="newState">new animator state</param>
    public void ChangeAnimatorState(int newState)
    {
        SomethingOpen = newState != 0;
        _animator.SetInteger(State, newState);
        AudioManager.Instance.PlaySecondSound("click");
    }

    /// <summary>
    /// Enable any gameobject (for small elements)
    /// </summary>
    /// <param name="gameObject">Any gameobject</param>
    public void EnableGameObject(GameObject gameObject)
    {
        gameObject.SetActive(true);
        SomethingOpen = true;
        AudioManager.Instance.PlaySecondSound("click");
    }

    /// <summary>
    /// Disable any gameobject (for small elements)
    /// </summary>
    /// <param name="gameObject">Any gameobject</param>
    public void DisableGameObject(GameObject gameObject)
    {
        gameObject.SetActive(false);
        SomethingOpen = false;
    }
}